<?php get_header(); ?>
<div class="clear"></div>


<section id="contact-page">
    <h1>Contact Us</h1>
    <div id="locations">
        <ul>
            <li id="set-srilanka-location" class="active"><img src="<?php bloginfo('template_url'); ?>/images/contact/srilanka.png" alt="" />Ceylon</li>
            <li id="set-japan-location"><img src="<?php bloginfo('template_url'); ?>/images/contact/japan.png" alt="" />Japan</li>
            <li id="set-us-location"><img src="<?php bloginfo('template_url'); ?>/images/contact/us.png" alt="" />United States</li>
        </ul>

        <div class="clear"></div>

        <div id="location-srilanka" class="location-detials">
        <div>
        <div>
                <h2>Address:</h2>
                <p>No:29,<br/>
                Ex Serviceman Building,<br/>
                Bristol Street,<br/>
                Colombo 01, Sri Lanka<br/>
                </p>
            </div>

            <div>
                <div>
                    <h2>Tel No:</h2>
                    <p>+94-11-3178-025</p>
                </div>
                <div>
                    <h2>E-mail:</h2>
                    <p>ceylon_gems@yahoo.com</p>
                </div>
            </div>
          </div>
                        <div id="location-map-srilanka" class="locate-google"></div>
        </div>


        <div id="location-japan" class="location-detials">
         <div>   <div>
                <h2>Address:</h2>
                <p>No:#,<br/>
                Some Street,<br/>
                City Name, Japan<br/>
                </p>
            </div>

            <div>
                <div>
                    <h2>Tel No:</h2>
                    <p>+94-11-3178-025</p>
                </div>
                <div>
                    <h2>E-mail:</h2>
                    <p>ceylon_gems@yahoo.co.jp</p>
                </div>
            </div>
            </div>
            <div id="location-map-japan" class="locate-google"></div>
        </div>

        <div id="location-us" class="location-detials">
           <div><div>
                <h2>Address:</h2>
                <p>No:#,<br/>
                Some us street,<br/>
                City, USA<br/>
                </p>
            </div>

            <div>
                <div>
                    <h2>Tel No:</h2>
                    <p>+94-11-3178-025</p>
                </div>
                <div>
                    <h2>E-mail:</h2>
                    <p>ceylon_gems@yahoo.co.us</p>
                </div>
            </div>
             </div>
            <div id="location-map-us" class="locate-google"></div>
        </div>
    </div><!--END locations-->
 <div class="clear"></div>
    <div class="divider"></div>
        <h1>Drop Us A Message</h1>
    <div id="contact-message">
        <form action="/nojili/wp-content/themes/nojili-beta/newsletterSignup.php" method="POST">
            <input type="hidden" name="contact" value="submit"/>
            <input type="hidden" name="redirect" value="<?php echo $_SERVER['REQUEST_URI'].'?'; ?>"/>
            <label for="message-name">Name:</label><br/>
            <input type="text" id="message-name" name="message-name" required="required"/>
            <br/><br/>

            <label for="message-email">Email:</label><br/>
            <input type="email" id="message-email" name="message-email" required="required" />
            <br/><br/>

            <label for="message-content">Message:</label><br/>
            <textarea name="message-content" id="message-content" cols="30" rows="10"></textarea>

            <br/>

            <button type="submit" name="message-submit" id="message-submit">Submit</button>
        </form>
    </div>
</section>

<?php get_footer(); ?>