
-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;


-- ---
-- Table 'image'
-- This table contains the URL for item images
-- ---

DROP TABLE IF EXISTS `image`;
		
CREATE TABLE `image` (
  `id` TINYINT NOT NULL AUTO_INCREMENT,
  `url` VARCHAR(120) NOT NULL,
  `description` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'This table contains the URL for item images';


-- ---
-- Table 'item'
-- This table contains information about the items(products)
-- ---

DROP TABLE IF EXISTS `item`;
		
CREATE TABLE `item` (
  `id` TINYINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(40) NOT NULL,
  `quantity_in_stock` VARCHAR(20) NOT NULL,
  `karat` VARCHAR(40) NOT NULL,
  `stone_description` VARCHAR(120) NOT NULL,
  `price` VARCHAR(10) NOT NULL,
  `serial_number` VARCHAR(40) NOT NULL,
  `image_id` TINYINT NULL DEFAULT NULL,
  `class_1` VARCHAR(40) NOT NULL,
  `class_2` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'This table contains information about the items(products)';

-- ---
-- Table 'address'
-- This table will store an individual customer''s address details
-- ---

DROP TABLE IF EXISTS `address`;
		
CREATE TABLE `address` (
  `id` TINYINT NOT NULL AUTO_INCREMENT,
  `address_line_1` VARCHAR(40) NOT NULL,
  `address_line_2` VARCHAR(40) NOT NULL,
  `suburb` VARCHAR(40) NOT NULL,
  `postal_code` VARCHAR(10) NOT NULL,
  `city` VARCHAR(40) NOT NULL,
  `country` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'This table will store an individual customer''s address detai';

-- ---
-- Table 'order'
-- This table stores details about individual orders
-- ---

DROP TABLE IF EXISTS `order`;
		
CREATE TABLE `order` (
  `id` TINYINT NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `status` VARCHAR(40) NOT NULL,
  `customer_id` TINYINT NULL DEFAULT NULL,
  `transaction_number` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'This table stores details about individual orders';

-- ---
-- Table 'customer'
-- This table stores information about individual customers
-- ---

DROP TABLE IF EXISTS `customer`;
		
CREATE TABLE `customer` (
  `id` TINYINT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(40) NULL,
  `last_name` VARCHAR(40) NULL,
  `address_id` TINYINT NULL DEFAULT NULL,
  `email` VARCHAR(120) NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'This table stores information about individual customers';

-- ---
-- Table 'item_order'
-- This table contains the transaction details
-- ---

DROP TABLE IF EXISTS `item_order`;
		
CREATE TABLE `item_order` (
  `id` TINYINT NOT NULL AUTO_INCREMENT,
  `item_id` TINYINT NOT NULL,
  `order_id` TINYINT NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'This table contains the transaction details';


-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `item` ADD FOREIGN KEY (image_id) REFERENCES `image` (`id`);
ALTER TABLE `order` ADD FOREIGN KEY (customer_id) REFERENCES `customer` (`id`);
ALTER TABLE `customer` ADD FOREIGN KEY (address_id) REFERENCES `address` (`id`);
ALTER TABLE `item_order` ADD FOREIGN KEY (item_id) REFERENCES `item` (`id`);
ALTER TABLE `item_order` ADD FOREIGN KEY (order_id) REFERENCES `order` (`id`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `item` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `address` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `order` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `customer` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `item_order` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `image` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `item` (`id`,`name`,`category`,`karat`,`stone_description`,`price`,`serial_number`,`image_id`,`class_1`,`class_2`) VALUES
-- ('','','','','','','','','','');
-- INSERT INTO `address` (`id`,`address_line_1`,`address_line_2`,`suburb`,`postal_code`,`city`,`country`) VALUES
-- ('','','','','','','');
-- INSERT INTO `order` (`id`,`date`,`status`,`customer_id`,`transaction_number`) VALUES
-- ('','','','','');
-- INSERT INTO `customer` (`id`,`first_name`,`last_name`,`address_id`,`email`) VALUES
-- ('','','','','');
-- INSERT INTO `item_order` (`id`,`item_id`,`order_id`) VALUES
-- ('','','');
-- INSERT INTO `image` (`id`,`url`,`description`) VALUES
-- ('','','');

-- ALTER TABLE TO REPLACE CATEGORY WITH QUANTITY IN STOCK
-- ALTER TABLE `item` CHANGE COLUMN `category` `quantity_in_stock` VARCHAR(20);

-- ALTER TABLE CUSTOMER TO ALLOW FIRST AND LAST NAME AS NULL
-- ALTER TABLE `customer` modify COLUMN `first_name` varchar(40);
-- ALTER TABLE `customer` modify COLUMN `last_name` varchar(40);