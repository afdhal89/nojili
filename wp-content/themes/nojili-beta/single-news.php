<?php get_header(); ?>
<div class="clear"></div>
<section id="single-page-news">
    <?php
//WordPress loop for custom post type
    while (have_posts()) : the_post();
        ?>
        <div>
            <h1><?php the_title(); ?></h1>
            <p class="post-date">- <?php echo get_the_date(); ?> -</p>

            <div class="news-content">
                <?php the_content(); ?>
            </div>
        </div>

        <?php
    endwhile;
    wp_reset_query();
    ?>

    <div class="clear"></div>
    <div class="hr"><hr /></div>

    <!--
    <div id="comments">
    <?php //comments_template();   ?>
    </div>
    -->

</section>

<?php
// Restore original Post Data
wp_reset_postdata();
?>

<?php get_footer(); ?>