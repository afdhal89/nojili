<?php
/* * ******************************************************************************************** */
/* Define Constants */
/* * ******************************************************************************************** */
define('THEMEROOT', get_stylesheet_directory_uri());
define('IMAGES', THEMEROOT . '/images');





/* * ******************************************************************************************** */
/* Add Menus */
/* * ******************************************************************************************** */

function register_my_menus() {
    register_nav_menus(array(
        'main-menu' => 'Main Menu',
        'category-menu' => 'Category Menu'
    ));
}

add_action('init', 'register_my_menus');





/* * ******************************************************************************************** */
/* Add Theme Support for Post Thumbnails */
/* * ******************************************************************************************** */
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(362, 209);
}

if (function_exists('add_image_size')) {
    add_image_size('featured', 400, 250, true); //true - we want to crop the image
    add_image_size('post-thumb', 200, 125, true);
}



/* * ******************************************************************************************** */
/* Load JS Files */
/* * ******************************************************************************************** */
add_action('wp_enqueue_scripts', 'load_custom_scripts');

function load_custom_scripts() {
    wp_enqueue_script('custom_script', THEMEROOT . '/js/custom.js', array('jquery'), true);
}

    ?>



<?php

//Control Excerpt Length using Filters
function custom_excerpt_length($length) {
    return 15;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);

// Make the "read more" link to the post
function new_excerpt_more($more) {
    return ' <a class="read-more" href="' . get_permalink(get_the_ID()) . '">' . __('(Read More...)', 'your-text-domain') . '</a>';
}

add_filter('excerpt_more', 'new_excerpt_more');

/* * ******************************************************************************************** */
/* Create a database object */
/* * ******************************************************************************************** */
require 'libs/medoo.php';

function createDbObject() {
    //echo 'calling create db object';
    global $databaseObject;
    $databaseObject = new medoo('nojilipro');
}

add_action('wp_loaded', 'createDbObject');


/* * ******************************************************************************************** */
/* Create News category */
/* * ******************************************************************************************** */
if(file_exists('../wp-admin/includes/taxonomy.php')) {
    
    require_once '../wp-admin/includes/taxonomy.php';
    
    function createNewsCategory() {

        $check = term_exists('News', 'category');

        if($check == 0 || $check == null) {
            wp_create_category('News');
        }


    }

    add_action('wp_loaded', 'createNewsCategory');

}
?>
