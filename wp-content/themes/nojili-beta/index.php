<?php get_header(); ?>

<div class="clear margin-top-20"></div>
<section id="home-main-content-body">
    <div id="home-main-content">

        <div id="content-slide-away">

            <!--<div id="productSlider">
            <header>
            <div>
            <h1>Latest Products</h1>
            </div>
            </header>
            <!-- thumbnail scroller markup begin -->
            <!--<div id="tS2" class="jThumbnailScroller">
            <div class="jTscrollerContainer">
            <div class="jTscroller">
            <?php /* if (have_posts()) : ?>
              <?php while (have_posts()) : the_post(); ?>
              <?php if (has_post_thumbnail()) : ?>

              <a href="<?php the_permalink(); ?>">
              <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
              <img src="<?php echo $url; ?>"/>
              <h4><?php the_title(); ?></h4>
              </a>
              <?php endif; ?>
              <?php endwhile; ?>
              <?php endif; */ ?>
            </div>
            </div>
            <a href="#" class="jTscrollerPrevButton"></a>
            <a href="#" class="jTscrollerNextButton"></a>
            </div>
            <!-- thumbnail scroller markup end -->
            <!--</div>    <!-- END productSlider -->

            <div id="product-slider">
                <!--
                <header>
                <div>
                <h1>Latest Products</h1>
                </div>
                </header>
                -->
                <!-- thumbnail scroller markup begin -->


                <section id="grid-gallery">
                    <ul id="gallery">

                    </ul>
                </section>
            </div>    <!-- END productSlider -->

            <div class="clear"></div>

            <div id="horoscope"  >
                <div id="horoscope-text">

                    <div>
                        <p>Get a Piece of <span>Jewellery</span> made <br/>for your <span>HOROSCOPE</span></p>
                        <!--  <p>DATE OF BIRTH</p> -->
                        <div class="md-modal md-effect-12" id="modal-12">
                            <div class="md-content">
                                <button class="md-close">Close</button>
                                <div>
                                    <img src="<?php bloginfo('template_url'); ?>/images/h_je.png" alt="" />
                                    <div id="dob">
                                        <form action="wp-content/themes/nojili-beta/newsletterSignup.php" method="POST">
                                            <input type="hidden" name="horoscope" value="submit"/>
                                            <input type="hidden" name="redirect" value="<?php echo $_SERVER['REQUEST_URI'].'?'; ?>"/>
                                            <label for="date-of-birth"> Date of Birth :</label>
                                            <script>
                                                if (jQuery.browser.webkit) {
                                                    document.write("<input id='date-of-birth' name='date-of-birth' type='date' placeholder='DD/MM/YYYY'>");

                                                }

                                                else {
                                                    document.write("<input type='text' id='date-picker' name='date-of-birth' placeholder='DD/MM/YYYY'>");
                                            //<input type="text" id="date-of-birth" placeholder="mm/dd/yyyy">
                                                }
                                            </script>
                                            <br/>
                                            <label for="birth-time"> Birth Time : </label>
                                            <script>
                                                if (jQuery.browser.webkit) {
                                                    document.write("<input id='birth-time' name='birth-time' type='time'  autocomplete='off'>");
                                            //<input id="birth-time" type="time" maxlength="10" placeholder="">
                                                }

                                                else {
                                                    document.write("<input type='text' name='birth-time'  class='time-selector' autocomplete='off' placeholder='HH:MM [Meridian]'  /> ");
                                            //<input type="text" name="time" />
                                                }
                                            </script>
                                            <br/>
                                            <label for="time-zone">Time Zone :</label>
                                            <select name="time-zone">
                                                <option timeZoneId="1" gmtAdjustment="GMT-12:00" useDaylightTime="0" value="-12">(GMT-12:00) International Date Line West</option>
                                                <option timeZoneId="2" gmtAdjustment="GMT-11:00" useDaylightTime="0" value="-11">(GMT-11:00) Midway Island, Samoa</option>
                                                <option timeZoneId="3" gmtAdjustment="GMT-10:00" useDaylightTime="0" value="-10">(GMT-10:00) Hawaii</option>
                                                <option timeZoneId="4" gmtAdjustment="GMT-09:00" useDaylightTime="1" value="-9">(GMT-09:00) Alaska</option>
                                                <option timeZoneId="5" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="-8">(GMT-08:00) Pacific Time (US & Canada)</option>
                                                <option timeZoneId="6" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="-8">(GMT-08:00) Tijuana, Baja California</option>
                                                <option timeZoneId="7" gmtAdjustment="GMT-07:00" useDaylightTime="0" value="-7">(GMT-07:00) Arizona</option>
                                                <option timeZoneId="8" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="-7">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
                                                <option timeZoneId="9" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="-7">(GMT-07:00) Mountain Time (US & Canada)</option>
                                                <option timeZoneId="10" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="-6">(GMT-06:00) Central America</option>
                                                <option timeZoneId="11" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="-6">(GMT-06:00) Central Time (US & Canada)</option>
                                                <option timeZoneId="12" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="-6">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
                                                <option timeZoneId="13" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="-6">(GMT-06:00) Saskatchewan</option>
                                                <option timeZoneId="14" gmtAdjustment="GMT-05:00" useDaylightTime="0" value="-5">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
                                                <option timeZoneId="15" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="-5">(GMT-05:00) Eastern Time (US & Canada)</option>
                                                <option timeZoneId="16" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="-5">(GMT-05:00) Indiana (East)</option>
                                                <option timeZoneId="17" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="-4">(GMT-04:00) Atlantic Time (Canada)</option>
                                                <option timeZoneId="18" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="-4">(GMT-04:00) Caracas, La Paz</option>
                                                <option timeZoneId="19" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="-4">(GMT-04:00) Manaus</option>
                                                <option timeZoneId="20" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="-4">(GMT-04:00) Santiago</option>
                                                <option timeZoneId="21" gmtAdjustment="GMT-03:30" useDaylightTime="1" value="-3.5">(GMT-03:30) Newfoundland</option>
                                                <option timeZoneId="22" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Brasilia</option>
                                                <option timeZoneId="23" gmtAdjustment="GMT-03:00" useDaylightTime="0" value="-3">(GMT-03:00) Buenos Aires, Georgetown</option>
                                                <option timeZoneId="24" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Greenland</option>
                                                <option timeZoneId="25" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Montevideo</option>
                                                <option timeZoneId="26" gmtAdjustment="GMT-02:00" useDaylightTime="1" value="-2">(GMT-02:00) Mid-Atlantic</option>
                                                <option timeZoneId="27" gmtAdjustment="GMT-01:00" useDaylightTime="0" value="-1">(GMT-01:00) Cape Verde Is.</option>
                                                <option timeZoneId="28" gmtAdjustment="GMT-01:00" useDaylightTime="1" value="-1">(GMT-01:00) Azores</option>
                                                <option timeZoneId="29" gmtAdjustment="GMT+00:00" useDaylightTime="0" value="0">(GMT+00:00) Casablanca, Monrovia, Reykjavik</option>
                                                <option timeZoneId="30" gmtAdjustment="GMT+00:00" useDaylightTime="1" value="0">(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London</option>
                                                <option timeZoneId="31" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                                                <option timeZoneId="32" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                                                <option timeZoneId="33" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                                                <option timeZoneId="34" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
                                                <option timeZoneId="35" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) West Central Africa</option>
                                                <option timeZoneId="36" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Amman</option>
                                                <option timeZoneId="37" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Athens, Bucharest, Istanbul</option>
                                                <option timeZoneId="38" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Beirut</option>
                                                <option timeZoneId="39" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Cairo</option>
                                                <option timeZoneId="40" gmtAdjustment="GMT+02:00" useDaylightTime="0" value="2">(GMT+02:00) Harare, Pretoria</option>
                                                <option timeZoneId="41" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
                                                <option timeZoneId="42" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Jerusalem</option>
                                                <option timeZoneId="43" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Minsk</option>
                                                <option timeZoneId="44" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Windhoek</option>
                                                <option timeZoneId="45" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Kuwait, Riyadh, Baghdad</option>
                                                <option timeZoneId="46" gmtAdjustment="GMT+03:00" useDaylightTime="1" value="3">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
                                                <option timeZoneId="47" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Nairobi</option>
                                                <option timeZoneId="48" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Tbilisi</option>
                                                <option timeZoneId="49" gmtAdjustment="GMT+03:30" useDaylightTime="1" value="3.5">(GMT+03:30) Tehran</option>
                                                <option timeZoneId="50" gmtAdjustment="GMT+04:00" useDaylightTime="0" value="4">(GMT+04:00) Abu Dhabi, Muscat</option>
                                                <option timeZoneId="51" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="4">(GMT+04:00) Baku</option>
                                                <option timeZoneId="52" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="4">(GMT+04:00) Yerevan</option>
                                                <option timeZoneId="53" gmtAdjustment="GMT+04:30" useDaylightTime="0" value="4.5">(GMT+04:30) Kabul</option>
                                                <option timeZoneId="54" gmtAdjustment="GMT+05:00" useDaylightTime="1" value="5">(GMT+05:00) Yekaterinburg</option>
                                                <option timeZoneId="55" gmtAdjustment="GMT+05:00" useDaylightTime="0" value="5">(GMT+05:00) Islamabad, Karachi, Tashkent</option>
                                                <option timeZoneId="56" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="5.5">(GMT+05:30) Sri Jayawardenapura</option>
                                                <option timeZoneId="57" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="5.5">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                                                <option timeZoneId="58" gmtAdjustment="GMT+05:45" useDaylightTime="0" value="5.75">(GMT+05:45) Kathmandu</option>
                                                <option timeZoneId="59" gmtAdjustment="GMT+06:00" useDaylightTime="1" value="6">(GMT+06:00) Almaty, Novosibirsk</option>
                                                <option timeZoneId="60" gmtAdjustment="GMT+06:00" useDaylightTime="0" value="6">(GMT+06:00) Astana, Dhaka</option>
                                                <option timeZoneId="61" gmtAdjustment="GMT+06:30" useDaylightTime="0" value="6.5">(GMT+06:30) Yangon (Rangoon)</option>
                                                <option timeZoneId="62" gmtAdjustment="GMT+07:00" useDaylightTime="0" value="7">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
                                                <option timeZoneId="63" gmtAdjustment="GMT+07:00" useDaylightTime="1" value="7">(GMT+07:00) Krasnoyarsk</option>
                                                <option timeZoneId="64" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                                                <option timeZoneId="65" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Kuala Lumpur, Singapore</option>
                                                <option timeZoneId="66" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
                                                <option timeZoneId="67" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Perth</option>
                                                <option timeZoneId="68" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Taipei</option>
                                                <option timeZoneId="69" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="9">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
                                                <option timeZoneId="70" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="9">(GMT+09:00) Seoul</option>
                                                <option timeZoneId="71" gmtAdjustment="GMT+09:00" useDaylightTime="1" value="9">(GMT+09:00) Yakutsk</option>
                                                <option timeZoneId="72" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="9.5">(GMT+09:30) Adelaide</option>
                                                <option timeZoneId="73" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="9.5">(GMT+09:30) Darwin</option>
                                                <option timeZoneId="74" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="10">(GMT+10:00) Brisbane</option>
                                                <option timeZoneId="75" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Canberra, Melbourne, Sydney</option>
                                                <option timeZoneId="76" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Hobart</option>
                                                <option timeZoneId="77" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="10">(GMT+10:00) Guam, Port Moresby</option>
                                                <option timeZoneId="78" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Vladivostok</option>
                                                <option timeZoneId="79" gmtAdjustment="GMT+11:00" useDaylightTime="1" value="11">(GMT+11:00) Magadan, Solomon Is., New Caledonia</option>
                                                <option timeZoneId="80" gmtAdjustment="GMT+12:00" useDaylightTime="1" value="12">(GMT+12:00) Auckland, Wellington</option>
                                                <option timeZoneId="81" gmtAdjustment="GMT+12:00" useDaylightTime="0" value="12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
                                                <option timeZoneId="82" gmtAdjustment="GMT+13:00" useDaylightTime="0" value="13">(GMT+13:00) Nuku'alofa</option>
                                            </select>
                                            <hr/>
                                            <br/>
                                            <label for="birth-place"> Place of Birth :</label>
                                            <br/>
                                            <label for="birth-place-country">&nbsp;&nbsp; Country :</label>

                                            <select name="country" id="country">
                                                <option value="" selected="selected">Select Country...</option>
                                                <option value="United States">United States</option>
                                                <option value="United Kingdom">United Kingdom</option>
                                                <option value="Afghanistan">Afghanistan</option>
                                                <option value="Albania">Albania</option>
                                                <option value="Algeria">Algeria</option>
                                                <option value="American Samoa">American Samoa</option>
                                                <option value="Andorra">Andorra</option>
                                                <option value="Angola">Angola</option>
                                                <option value="Anguilla">Anguilla</option>
                                                <option value="Antarctica">Antarctica</option>
                                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                <option value="Argentina">Argentina</option>
                                                <option value="Armenia">Armenia</option>
                                                <option value="Aruba">Aruba</option>
                                                <option value="Australia">Australia</option>
                                                <option value="Austria">Austria</option>
                                                <option value="Azerbaijan">Azerbaijan</option>
                                                <option value="Bahamas">Bahamas</option>
                                                <option value="Bahrain">Bahrain</option>
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="Barbados">Barbados</option>
                                                <option value="Belarus">Belarus</option>
                                                <option value="Belgium">Belgium</option>
                                                <option value="Belize">Belize</option>
                                                <option value="Benin">Benin</option>
                                                <option value="Bermuda">Bermuda</option>
                                                <option value="Bhutan">Bhutan</option>
                                                <option value="Bolivia">Bolivia</option>
                                                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                <option value="Botswana">Botswana</option>
                                                <option value="Bouvet Island">Bouvet Island</option>
                                                <option value="Brazil">Brazil</option>
                                                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                <option value="Bulgaria">Bulgaria</option>
                                                <option value="Burkina Faso">Burkina Faso</option>
                                                <option value="Burundi">Burundi</option>
                                                <option value="Cambodia">Cambodia</option>
                                                <option value="Cameroon">Cameroon</option>
                                                <option value="Canada">Canada</option>
                                                <option value="Cape Verde">Cape Verde</option>
                                                <option value="Cayman Islands">Cayman Islands</option>
                                                <option value="Central African Republic">Central African Republic</option>
                                                <option value="Chad">Chad</option>
                                                <option value="Chile">Chile</option>
                                                <option value="China">China</option>
                                                <option value="Christmas Island">Christmas Island</option>
                                                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                <option value="Colombia">Colombia</option>
                                                <option value="Comoros">Comoros</option>
                                                <option value="Congo">Congo</option>
                                                <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                                <option value="Cook Islands">Cook Islands</option>
                                                <option value="Costa Rica">Costa Rica</option>
                                                <option value="Cote D'ivoire">Cote D'ivoire</option>
                                                <option value="Croatia">Croatia</option>
                                                <option value="Cuba">Cuba</option>
                                                <option value="Cyprus">Cyprus</option>
                                                <option value="Czech Republic">Czech Republic</option>
                                                <option value="Denmark">Denmark</option>
                                                <option value="Djibouti">Djibouti</option>
                                                <option value="Dominica">Dominica</option>
                                                <option value="Dominican Republic">Dominican Republic</option>
                                                <option value="Ecuador">Ecuador</option>
                                                <option value="Egypt">Egypt</option>
                                                <option value="El Salvador">El Salvador</option>
                                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                <option value="Eritrea">Eritrea</option>
                                                <option value="Estonia">Estonia</option>
                                                <option value="Ethiopia">Ethiopia</option>
                                                <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                <option value="Faroe Islands">Faroe Islands</option>
                                                <option value="Fiji">Fiji</option>
                                                <option value="Finland">Finland</option>
                                                <option value="France">France</option>
                                                <option value="French Guiana">French Guiana</option>
                                                <option value="French Polynesia">French Polynesia</option>
                                                <option value="French Southern Territories">French Southern Territories</option>
                                                <option value="Gabon">Gabon</option>
                                                <option value="Gambia">Gambia</option>
                                                <option value="Georgia">Georgia</option>
                                                <option value="Germany">Germany</option>
                                                <option value="Ghana">Ghana</option>
                                                <option value="Gibraltar">Gibraltar</option>
                                                <option value="Greece">Greece</option>
                                                <option value="Greenland">Greenland</option>
                                                <option value="Grenada">Grenada</option>
                                                <option value="Guadeloupe">Guadeloupe</option>
                                                <option value="Guam">Guam</option>
                                                <option value="Guatemala">Guatemala</option>
                                                <option value="Guinea">Guinea</option>
                                                <option value="Guinea-bissau">Guinea-bissau</option>
                                                <option value="Guyana">Guyana</option>
                                                <option value="Haiti">Haiti</option>
                                                <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                                <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                <option value="Honduras">Honduras</option>
                                                <option value="Hong Kong">Hong Kong</option>
                                                <option value="Hungary">Hungary</option>
                                                <option value="Iceland">Iceland</option>
                                                <option value="India">India</option>
                                                <option value="Indonesia">Indonesia</option>
                                                <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                <option value="Iraq">Iraq</option>
                                                <option value="Ireland">Ireland</option>
                                                <option value="Israel">Israel</option>
                                                <option value="Italy">Italy</option>
                                                <option value="Jamaica">Jamaica</option>
                                                <option value="Japan">Japan</option>
                                                <option value="Jordan">Jordan</option>
                                                <option value="Kazakhstan">Kazakhstan</option>
                                                <option value="Kenya">Kenya</option>
                                                <option value="Kiribati">Kiribati</option>
                                                <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                                <option value="Korea, Republic of">Korea, Republic of</option>
                                                <option value="Kuwait">Kuwait</option>
                                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                                <option value="Latvia">Latvia</option>
                                                <option value="Lebanon">Lebanon</option>
                                                <option value="Lesotho">Lesotho</option>
                                                <option value="Liberia">Liberia</option>
                                                <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                <option value="Liechtenstein">Liechtenstein</option>
                                                <option value="Lithuania">Lithuania</option>
                                                <option value="Luxembourg">Luxembourg</option>
                                                <option value="Macao">Macao</option>
                                                <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                                <option value="Madagascar">Madagascar</option>
                                                <option value="Malawi">Malawi</option>
                                                <option value="Malaysia">Malaysia</option>
                                                <option value="Maldives">Maldives</option>
                                                <option value="Mali">Mali</option>
                                                <option value="Malta">Malta</option>
                                                <option value="Marshall Islands">Marshall Islands</option>
                                                <option value="Martinique">Martinique</option>
                                                <option value="Mauritania">Mauritania</option>
                                                <option value="Mauritius">Mauritius</option>
                                                <option value="Mayotte">Mayotte</option>
                                                <option value="Mexico">Mexico</option>
                                                <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                <option value="Monaco">Monaco</option>
                                                <option value="Mongolia">Mongolia</option>
                                                <option value="Montserrat">Montserrat</option>
                                                <option value="Morocco">Morocco</option>
                                                <option value="Mozambique">Mozambique</option>
                                                <option value="Myanmar">Myanmar</option>
                                                <option value="Namibia">Namibia</option>
                                                <option value="Nauru">Nauru</option>
                                                <option value="Nepal">Nepal</option>
                                                <option value="Netherlands">Netherlands</option>
                                                <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                <option value="New Caledonia">New Caledonia</option>
                                                <option value="New Zealand">New Zealand</option>
                                                <option value="Nicaragua">Nicaragua</option>
                                                <option value="Niger">Niger</option>
                                                <option value="Nigeria">Nigeria</option>
                                                <option value="Niue">Niue</option>
                                                <option value="Norfolk Island">Norfolk Island</option>
                                                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                <option value="Norway">Norway</option>
                                                <option value="Oman">Oman</option>
                                                <option value="Pakistan">Pakistan</option>
                                                <option value="Palau">Palau</option>
                                                <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                <option value="Panama">Panama</option>
                                                <option value="Papua New Guinea">Papua New Guinea</option>
                                                <option value="Paraguay">Paraguay</option>
                                                <option value="Peru">Peru</option>
                                                <option value="Philippines">Philippines</option>
                                                <option value="Pitcairn">Pitcairn</option>
                                                <option value="Poland">Poland</option>
                                                <option value="Portugal">Portugal</option>
                                                <option value="Puerto Rico">Puerto Rico</option>
                                                <option value="Qatar">Qatar</option>
                                                <option value="Reunion">Reunion</option>
                                                <option value="Romania">Romania</option>
                                                <option value="Russian Federation">Russian Federation</option>
                                                <option value="Rwanda">Rwanda</option>
                                                <option value="Saint Helena">Saint Helena</option>
                                                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                <option value="Saint Lucia">Saint Lucia</option>
                                                <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                                <option value="Samoa">Samoa</option>
                                                <option value="San Marino">San Marino</option>
                                                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                <option value="Saudi Arabia">Saudi Arabia</option>
                                                <option value="Senegal">Senegal</option>
                                                <option value="Serbia and Montenegro">Serbia and Montenegro</option>
                                                <option value="Seychelles">Seychelles</option>
                                                <option value="Sierra Leone">Sierra Leone</option>
                                                <option value="Singapore">Singapore</option>
                                                <option value="Slovakia">Slovakia</option>
                                                <option value="Slovenia">Slovenia</option>
                                                <option value="Solomon Islands">Solomon Islands</option>
                                                <option value="Somalia">Somalia</option>
                                                <option value="South Africa">South Africa</option>
                                                <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                                <option value="Spain">Spain</option>
                                                <option value="Sri Lanka">Sri Lanka</option>
                                                <option value="Sudan">Sudan</option>
                                                <option value="Suriname">Suriname</option>
                                                <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                <option value="Swaziland">Swaziland</option>
                                                <option value="Sweden">Sweden</option>
                                                <option value="Switzerland">Switzerland</option>
                                                <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                                <option value="Tajikistan">Tajikistan</option>
                                                <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                <option value="Thailand">Thailand</option>
                                                <option value="Timor-leste">Timor-leste</option>
                                                <option value="Togo">Togo</option>
                                                <option value="Tokelau">Tokelau</option>
                                                <option value="Tonga">Tonga</option>
                                                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                <option value="Tunisia">Tunisia</option>
                                                <option value="Turkey">Turkey</option>
                                                <option value="Turkmenistan">Turkmenistan</option>
                                                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                <option value="Tuvalu">Tuvalu</option>
                                                <option value="Uganda">Uganda</option>
                                                <option value="Ukraine">Ukraine</option>
                                                <option value="United Arab Emirates">United Arab Emirates</option>
                                                <option value="United Kingdom">United Kingdom</option>
                                                <option value="United States">United States</option>
                                                <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                <option value="Uruguay">Uruguay</option>
                                                <option value="Uzbekistan">Uzbekistan</option>
                                                <option value="Vanuatu">Vanuatu</option>
                                                <option value="Venezuela">Venezuela</option>
                                                <option value="Viet Nam">Viet Nam</option>
                                                <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                <option value="Western Sahara">Western Sahara</option>
                                                <option value="Yemen">Yemen</option>
                                                <option value="Zambia">Zambia</option>
                                                <option value="Zimbabwe">Zimbabwe</option>
                                            </select>
                                            <br/>
                                            <label for="birth-place-city">&nbsp;&nbsp; City :</label>
                                            <input id="birth-place-city" name="birth-place-city" type="text" maxlength="10" placeholder="" >
                                            <br/>

                                            
                                            <label for="name">Name :</label>
                                            <input id="name" name="name" type="text" autocomplete="off"></br>
                                            
                                            <label for="email">Email :</label>
                                            <input id="email" name="email" type="email" autocomplete="off">

                                            <button class="submit">Submit</button>
                                        </form>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <a  id="sign-up-jewel" class="md-trigger" data-modal="modal-12" >Sign Up</a>
                        <div class="md-overlay"></div><!-- the overlay element -->
                    </div>

                    <div class="clear"></div>

                    <!--
                    <div id="dob">
                    <form>
                    <input id="day1" type="number" maxlength="1" placeholder="D">
                    <input id="day2" type="number" maxlength="1" placeholder="D"><span>&nbsp;-&nbsp;</span><input id="month1" type="number" maxlength="1" placeholder="M">
                    <input id="month2" type="number" maxlength="1" placeholder="M"><span>&nbsp;-&nbsp;</span><input id="year1" type="number" maxlength="1" placeholder="Y">
                    <input id="year2" type="number" maxlength="1" placeholder="Y">
                    <input id="year3" type="number" maxlength="1" placeholder="Y">
                    <input id="year4" type="number" maxlength="1" placeholder="Y">
                    <button id="submit-dob" type="submit">Submit</button>
                    </form>
                    </div>
                    -->
                </div><!-- END horoscope-text -->


            </div><!--END horoscope-->


            <div id="latest-news">
                <!--
                <header>
                <h1>Latest News</h1>
                </header>
                -->
                <div class="clear"></div>

                <div>
                    <div id="latest-news-container">
                        <div>

                            <?php
                            $args = array('category_name' => 'News',
                                'numberposts' => 3,
                                'offset' => 0,
                                'orderby' => 'post_date',
                                'order' => 'DESC',
                                'post_type' => 'post',
                                'post_status' => 'draft, publish, future, pending, private',
                                'suppress_filters' => true);
                            $postslist = get_posts($args);
                            foreach ($postslist as $post) : setup_postdata($post);
                                ?>
                                <article>
                                    <div class="latest-post-date">
                                        <div class="date"><?php echo get_the_date('j'); ?></div>
                                        <div class="month"><?php echo get_the_date('F'); ?></div>
                                        <div class="year"><?php echo get_the_date('Y'); ?></div>
                                    </div>

                                    <?php if (has_post_thumbnail()) : ?>
                                        <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
                                        <img src="<?php echo $url; ?>"/>
                                    <?php else : ?>
                                        <img src="<?php bloginfo('template_url'); ?>/images/latest-news-icon.png"/>
                                    <?php endif ?>
                                    <div>
                                        <h2><?php the_title(); ?></h2>
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <br/>
                                    <!-- <img src="<?php echo $url; ?>"/>-->
                                    <!-- <p><?php the_excerpt(); ?></p>-->
                                    <!-- <?php echo the_content(); ?>-->
                                </article>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>

            </div><!--END latest-news-->

        </div><!--END content-slide-away-->

    </div><!--END home-main-content-->

    <div id="gems-container">
    <h1>Gems</h1>
        <section class="tabs">
            <input id="tab-1" type="radio" name="radio-set" class="tab-selector-1" checked="checked" />
            <label for="tab-1" class="tab-label-1">Unheated Stones</label>

            <input id="tab-2" type="radio" name="radio-set" class="tab-selector-2" />
            <label for="tab-2" class="tab-label-2">Heated Stones</label>

<!--<input id="tab-3" type="radio" name="radio-set" class="tab-selector-3" />
<label for="tab-3" class="tab-label-3"></label>

<input id="tab-4" type="radio" name="radio-set" class="tab-selector-4" />
<label for="tab-4" class="tab-label-4">Earrings</label>

<input id="tab-5" type="radio" name="radio-set" class="tab-selector-5" />
<label for="tab-5" class="tab-label-5">Earrings</label>-->

            <div class="clear-shadow"></div>

            <div class="content">
                <!--
                FORMAT:
                <a href="link to single post page">
                <img src="link to image" />
                <h4>Name of the item</h4>
                Price of the item (plain HTML)
                </a>
                -->
                <div id="gems_unheated" class="content-1">
                    <!--<?php
//$args = array('category_name' => 'Gems', 'numberposts' => -1, 'orderby' => 'post_date');
//$postslist = get_posts($args);
//foreach ($postslist as $post) : setup_postdata($post);
                    ?>
                    <?php //if (has_post_thumbnail()) : ?>
                    <a href="<?php //the_permalink();             ?>">
                    <?php //$url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));  ?>
                    <img src="<?php //echo $url;             ?>"/>
                    <h4><?php //the_title();              ?></h4>
                    <?php
//$product_price = esc_html(get_post_meta($post->ID, 'product_price', true));
                    ?>
                    <?php
//if ($product_price != '') {
//    echo "
//        <p>$<span>$product_price</span></p>
//        ";
//}
                    ?>
                    </a>
                    <?php //endif;   ?>
                    <?php //endforeach; ?>-->
                </div>

                <div id="gems_heated" class="content-2">
                    <!--<?php
//$args = array('category_name' => 'Gems', 'numberposts' => -1, 'orderby' => 'post_date');
//$postslist = get_posts($args);
//foreach ($postslist as $post) : setup_postdata($post);
                    ?>
                    <?php //if (has_post_thumbnail()) : ?>
                    <a href="<?php //the_permalink();            ?>">
                    <?php //$url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));   ?>
                    <img src="<?php //echo $url;            ?>"/>
                    <h4><?php //the_title();              ?></h4>
                    <?php
//$product_price = esc_html(get_post_meta($post->ID, 'product_price', true));
                    ?>
                    <?php
//if ($product_price != '') {
//    echo "
//        <p>$<span>$product_price</span></p>
//        ";
//}
                    ?>
                    </a>
                    <?php //endif;  ?>
                    <?php //endforeach;  ?>-->
                </div>
            </div><!--END content-->
        </section>
    </div><!--END gems-container-->

    <div id="jewellery-container">
        <h1>Jewelleries</h1>
        <section class="tabs">
            <input id="tab-1" type="radio" name="radio-set2" class="tab-selector-1" checked="checked" />
            <label for="tab-1" class="tab-label-1">Pendant</label>

            <input id="tab-2" type="radio" name="radio-set2" class="tab-selector-2" />
            <label for="tab-2" class="tab-label-2">Rings</label>

<!--<input id="tab-3" type="radio" name="radio-set2" class="tab-selector-3" />
<label for="tab-3" class="tab-label-3">Rings</label>

<input id="tab-4" type="radio" name="radio-set2" class="tab-selector-4" />
<label for="tab-4" class="tab-label-4">Necklaces</label>

<input id="tab-5" type="radio" name="radio-set2" class="tab-selector-5" />
<label for="tab-5" class="tab-label-5">Bracelets</label>-->

            <div class="clear-shadow"></div>

            <div class="content">
                <!--
                FORMAT:
                <a href="link to single post page">
                <img src="link to image" />
                <h4>Name of the item</h4>
                </a>
                -->
                <div id="jewellery_pendant" class="content-1">
                    <?php
//$args = array('category_name' => 'Jewellery', 'numberposts' => -1, 'orderby' => 'post_date');
//$postslist = get_posts($args);
//foreach ($postslist as $post) : setup_postdata($post);
                    ?>
                    <!--<?php //if (has_post_thumbnail()) :            ?>
                    <a href="<?php //the_permalink();             ?>">
                    <?php //$url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));   ?>
                    <img src="<?php //echo $url;             ?>"/>
                    <h4><?php //the_title();             ?></h4>
                    <?php
//$product_price = esc_html(get_post_meta($post->ID, 'product_price', true));
                    ?>
                    <?php
//if ($product_price != '') {
//  echo "
//<p>$<span>$product_price</span></p>
//";
//}
                    ?>
                    </a>
                    <?php //endif;  ?>
                    <?php //endforeach; ?>-->
                </div>

                <div id="jewellery_ring" class="content-2">
                    <!--<?php
//$args = array('category_name' => 'Jewellery', 'numberposts' => -1, 'orderby' => 'post_date');
//$postslist = get_posts($args);
//foreach ($postslist as $post) : setup_postdata($post);
                    ?>
                    <?php //if (has_post_thumbnail()) : ?>
                    <a href="<?php //the_permalink();            ?>">
                    <?php //$url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));  ?>
                    <img src="<?php //echo $url;            ?>"/>
                    <h4><?php //the_title();              ?></h4>
                    <?php
//$product_price = esc_html(get_post_meta($post->ID, 'product_price', true));
                    ?>
                    <?php
//if ($product_price != '') {
//  echo "
//<p>$<span>$product_price</span></p>
//";
//}
                    ?>
                    </a>
                    <?php //endif;  ?>
                    <?php //endforeach; ?>-->
                </div>

                <!--<div class="content-5">
                <?php
//$args = array('category_name' => 'Jewellery', 'numberposts' => -1, 'orderby' => 'post_date');
//$postslist = get_posts($args);
//foreach ($postslist as $post) : setup_postdata($post);
                ?>
                <?php //if (has_post_thumbnail()) : ?>
                <a href="<?php //the_permalink();            ?>">
                <?php //$url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));  ?>
                <img src="<?php //echo $url;            ?>"/>
                <h4><?php //the_title();             ?></h4>
                <?php
//$product_price = esc_html(get_post_meta($post->ID, 'product_price', true));
                ?>
                <?php
//if ($product_price != '') {
//  echo "
//<p>$<span>$product_price</span></p>
//";
//}
                ?>
                </a>
                <?php //endif;  ?>
                <?php //endforeach; ?>
                </div>-->

                <!--<div class="content-5">
                <?php
//$args = array('category_name' => 'Jewellery', 'numberposts' => -1, 'orderby' => 'post_date');
//$postslist = get_posts($args);
//foreach ($postslist as $post) : setup_postdata($post);
                ?>
                <?php //if (has_post_thumbnail()) : ?>
                <a href="<?php //the_permalink();            ?>">
                <?php //$url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));  ?>
                <img src="<?php //echo $url;            ?>"/>
                <h4><?php //the_title();             ?></h4>
                <?php
//$product_price = esc_html(get_post_meta($post->ID, 'product_price', true));
                ?>
                <?php
//if ($product_price != '') {
//  echo "
//<p>$<span>$product_price</span></p>
//";
//}
                ?>
                </a>
                <?php //endif;  ?>
                <?php //endforeach; ?>
                </div>-->

                <!--<div class="content-5">
                <?php
//$args = array('category_name' => 'Jewellery', 'numberposts' => -1, 'orderby' => 'post_date');
//$postslist = get_posts($args);
//foreach ($postslist as $post) : setup_postdata($post);
                ?>
                <?php //if (has_post_thumbnail()) :  ?>
                <a href="<?php //the_permalink();            ?>">
                <?php //$url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));  ?>
                <img src="<?php //echo $url;            ?>"/>
                <h4><?php //the_title();             ?></h4>
                <?php
//$product_price = esc_html(get_post_meta($post->ID, 'product_price', true));
                ?>
                <?php
//if ($product_price != '') {
//  echo "
//<p>$<span>$product_price</span></p>
//";
//}
                ?>
                </a>
                <?php //endif;  ?>
                <?php //endforeach; ?>
                </div>-->

            </div><!--END content-->
        </section>
    </div><!--END jewellery-container-->
</section>

<div class="clear"></div>


<div class="clear"></div>
<div id="test">

</div>
<script>

</script>
<?php get_footer(); ?>
