<?php

require_once 'libs/swiftmailer/swift_required.php';
require_once 'libs/medoo.php';

if($_SERVER['REQUEST_METHOD'] == "POST") {
    //var_dump($_POST);
    if(isset($_POST['newsletterEmail'])) {
        newsletterNotification($_POST);
    }
    if(isset($_POST['horoscope'])) { 
        horoscopeNotification($_POST);
    }
    if(isset($_POST['contact'])) { 
        contactNotification($_POST);
    }
}

//$test['newsletterEmail'] = "test.p@gmail.com";

//newsletterNotification($test);

function newsletterNotification($data) {
    $db = new medoo('nojilipro');
    $result = $db->insert("customer", ["email" => $data['newsletterEmail']]);
    //echo $db->last_query();
    
    if($result != null) {
        $body = "Hello, \n\n You have a new newsletter sign up on the website. The following email address was sent to be included in your mailing list. \n\nEmail: " .                 $data['newsletterEmail'] .  ". \n\n Have a good day!";
    
        $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
          ->setUsername('throwaway.dee@gmail.com')
          ->setPassword('throw.dee');

        $mailer = Swift_Mailer::newInstance($transport);

        $message = Swift_Message::newInstance('Newsletter Signup Notification')
          ->setFrom(array(''.$data['newsletterEmail'] => ''.$data['newsletterEmail']))
          ->setTo(array('deleepa.p@gmail.com'))
          ->setBody($body);

        $result = $mailer->send($message);

        echo 'true';

    }
    else {
        echo 'false';
    }
}

function horoscopeNotification($data) {
    //echo 'in horo notif</br>';
    $body = "Hello, \n\n You have a new request for a horoscope related item. The following are the details submitted: \n" . 
            "Name: " . $data['name'] .
            "\nDate of birth: " . $data['date-of-birth'] .
            "\nBirth time: " . $data['birth-time'] . 
            "\nTime zone: " . $data['time-zone'] . 
            "\nCountry: " . $data['country'] . 
            "\nCity: " . $data['birth-place-city'] . 
            "\nEmail: " . $data['email'] . 
            "\n\n Please note that this information is not being stored on the database. \n\nHave a nice day!";

    $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
      ->setUsername('throwaway.dee@gmail.com')
      ->setPassword('throw.dee');

    $mailer = Swift_Mailer::newInstance($transport);

    $message = Swift_Message::newInstance('Horoscope Request Notification')
      ->setFrom(array(''.$data['email'] => ''.$data['name']))
      ->setTo(array('deleepa.p@gmail.com'))
      ->setBody($body);

    $result = $mailer->send($message);
    
    $url = $data['redirect'];
    
    header("Location: ".$url."horoscope=success");
}

function contactNotification($data) {
    //echo 'in horo notif</br>';
    $body = "Hello, \n\n You have a new contact request: \n" . 
            "Name: " . $data['message-name'] .
            "\nMessage " . $data['message-content'] .
            "\nSender Email: " . $data['message-email'] . 
            "\n\n Please note that this information is not being stored on the database. \n\nHave a nice day!";

    $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
      ->setUsername('throwaway.dee@gmail.com')
      ->setPassword('throw.dee');

    $mailer = Swift_Mailer::newInstance($transport);

    $message = Swift_Message::newInstance('Contact Notification')
      ->setFrom(array(''.$data['message-email'] => ''.$data['message-name']))
      ->setTo(array('deleepa.p@gmail.com'))
      ->setBody($body);

    $result = $mailer->send($message);
    
    $url = $data['redirect'];
    
    header("Location: ".$url."contact=success");
}
?>