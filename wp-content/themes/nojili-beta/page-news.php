<?php get_header(); ?>
<div class="clear"></div>
<section id="news-page">
    <header>
        <h1>News</h1>
    </header>
    <?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array('category_name' => 'News',
        'numberposts' => -1,
        'offset' => 0,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'post',
        'post_status' => 'draft, publish, future, pending, private',
        'suppress_filters' => true,
        'paged' => $paged,
        'posts_per_page' => 6);
    $postslist = get_posts($args);
    query_posts($args);
    foreach ($postslist as $post) : setup_postdata($post);
        ?>

        <article>
            <div class="post-date">
                <div class="date"><?php echo get_the_date('j'); ?></div>
                <div class="month"><?php echo get_the_date('F'); ?></div>
                <div class="year"><?php echo get_the_date('Y'); ?></div>
            </div>

            <?php if (has_post_thumbnail()) : ?>
                <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
                <img src="<?php echo $url; ?>"/>
            <?php else : ?>
                <img src="<?php bloginfo('template_url'); ?>/images/latest-news-icon.png"/>
            <?php endif ?>
            <div>
                <h2><?php the_title(); ?></h2>
                <?php the_excerpt(); ?>
            </div>
            <br/>
            <!-- <img src="<?php echo $url; ?>"/>-->
            <!-- <p><?php the_excerpt(); ?></p>-->
            <!-- <?php echo the_content(); ?>-->
        </article>
    <?php endforeach; ?>
    <?php
    global $wp_query;

    if ($wp_query->max_num_pages > 1) :
        ?>
        <ul class="news-nav">
            <li><?php previous_posts_link('&larr; Previous Page', $wp_query->max_num_pages); ?></li>
            <li><?php next_posts_link('Next Page &rarr;', $wp_query->max_num_pages); ?></li>
        </ul>
    <?php endif; ?>
</section>
<?php get_footer(); ?>