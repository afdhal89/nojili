<div class="clear"></div>
<footer id="footer">
<nav id="footer-nav">
<?php
wp_nav_menu(array(
'theme_location' => 'main-menu',
'container' => '',
'menu_class' => 'inline'
));
?>
</nav>

<div class="clear"></div>

<div class="divider-holder2">
<div class="divider2"></div>
</div>

<div id="footer_socialmedia">
<div id="social_media">
<p>Follow Us:         <br/>
<a href="https://plus.google.com/">
<img src="<?php bloginfo('template_url'); ?>/images/gplus-icon.png" class="img-responsive" >
</a>
<a href="https://twitter.com/">
<img src="<?php bloginfo('template_url'); ?>/images/twitter-icon.png" class="img-responsive" >
</a>
</p>
</div>

<div id="newsletter">
<p>Sign up for our newsletter</p>
<input type="text" id="newsletter-email" placeholder="Your Email">

</div> <!-- end of newsletter -->
</div><!-- end of footer_socialmedia     -->
</footer>
<?php
wp_footer();
$jewelleryJson = get_bloginfo('url') . "/wp-content/json_data/allJewellery.json";
$gemJson = get_bloginfo('url') . "/wp-content/json_data/allGems.json";
$topTenJson = get_bloginfo('url') . "/wp-content/json_data/latestTenItems.json";
$newsletter = get_bloginfo('template_url') . "/newsletterSignup.php";
?>
<script>

var gemJson = "<?php echo $gemJson; ?>";
var jewelleryJson = "<?php echo $jewelleryJson; ?>";
var topTenJson = "<?php echo $topTenJson; ?>";
var newsletterURL = "<?php echo $newsletter; ?>";
var homeURL = "<?php echo get_home_url() . '/'; ?>";
var currentURL = "<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>";

//var gemSelect = document.getElementbyId("gemSelect");
//gemSelect.addEventListener('click', function(){console.log("click detected");})

jQuery.ajax({
type: "POST",
url: topTenJson //change URL to get the top files
}).done(function(data) {
console.log("success loading JSON");
populateScroller(data);
//gemData = data;
});

function populateScroller(data) {
console.log(data);
console.log("in populate Scroller");
var div = document.getElementById("gallery");
var tempStr = "";
for (var i = 0; i < data.length; i++) {
tempStr += "<li><a href='<?php echo get_bloginfo('url') . '/single-product?'; ?>serial=" + data[i]['serial_number'] + "'></a><img src='nojili/" + data[i]["url"] + "'/><div class='overLayer'></div><div class='infoLayer'><ul><li><h2 class='clicktester'>" + data[i]["name"] + "</h2></li><li><p>" + data[i]["price"] + " $</p></li></ul></div></li>";
//console.log(tempStr);
}
div.innerHTML = tempStr;

//populate the Gems section
jQuery.ajax({
type: "POST",
url: gemJson
}).done(function(data) {
console.log("success loading JSON");
populateGems(data);
//gemData = data;
});
}

function populateGems(data) {

var unheated = document.getElementById("gems_unheated");
var heated = document.getElementById("gems_heated");

for (var i = 0; i < data.length; i++) {

var tempStr = "<a href='<?php echo get_bloginfo('url') . '/single-product?'; ?>serial=" + data[i]["serial_number"] + "'><img src='nojili/" +
data[i]["url"] + "'/><h4>" + data[i]["name"] + "</h4> <p>$<span>" + data[i]["price"] + "</spam></p></a>";
console.log(data[i]["class_2"]);
if (data[i]["class_2"] == "unheated") {
unheated.innerHTML += tempStr;
}
else if (data[i]["class_2"] == "heated") {
heated.innerHTML += tempStr;
}
}

//populate the Jewellery section
jQuery.ajax({
type: "POST",
url: jewelleryJson
}).done(function(data) {
console.log("success loading JSON");
populateJewellery(data);
//gemData = data;
});
}

function populateJewellery(data) {

var pendant = document.getElementById("jewellery_pendant");
var ring = document.getElementById("jewellery_ring");

for (var i = 0; i < data.length; i++) {

var tempStr = "<a href='<?php echo get_bloginfo('url') . '/single-product?'; ?>serial=" + data[i]["serial_number"] + "'><img src='nojili/" +
data[i]["url"] + "'/><h4>" + data[i]["name"] + "</h4> <p>$<span>" + data[i]["price"] + "</spam></p></a>";
console.log(data[i]["class_2"]);
if (data[i]["class_2"] == "pendant") {
pendant.innerHTML += tempStr;
}
else if (data[i]["class_2"] == "ring") {
ring.innerHTML += tempStr;
}
}
return true;
}

function populateSingleProductPageScroller() {
}

function showGemsTab(){

if (jQuery("#jewellery-container").is(':visible')) {
jQuery("#jewellery-container").toggle('slide', {direction: 'left'}, 700);
jQuery("#gems-container").toggle('slide', {direction: 'right'}, 700);
}
else {
jQuery("#gems-container").toggle('slide', {direction: 'right'}, 700);
jQuery("#content-slide-away").toggle('slide', {direction: 'left'}, 700);
}
}

//jQuery("#gemSelect").click(function() {
 //showGemsTab();
// jQuery("#productSlider").toggle('slide');
//jQuery("#gems-container").toggle('slide', { direction: 'right' }, 700);
//slideToggle();
//.toggle( "slide" );
//.toggle( "drop" );
//.toggle( "fold" );
//jQuery("#close-nav").show("slow");
//});
/*jQuery("#close-nav").click(function (){
jQuery(".main-nav").toggle("slow");
jQuery("#close-nav").hide("slow");
});*/




function showJewelleryTab(){
  if (jQuery("#gems-container").is(':visible')) {
jQuery("#gems-container").toggle('slide', {direction: 'left'}, 700);
jQuery("#jewellery-container").toggle('slide', {direction: 'right'}, 700);
}
else {
jQuery("#jewellery-container").toggle('slide', {direction: 'right'}, 700);
jQuery("#content-slide-away").toggle('slide', {direction: 'left'}, 700);
}
}

jQuery("#jewellerySelect").click(function() {
    if(homeURL == currentURL) {
        showJewelleryTab();
    }
    else {
        var showExists = currentURL.search("show");
        showExists = parseInt(showExists);
        if(showExists > 0) {
            showJewelleryTab();
        }
        else {
            window.location.assign(homeURL + "?show=jewellery");
        }
    }
});
jQuery("#gemSelect").click(function() {

    if(homeURL == currentURL) {
        showGemsTab();
    }
    else {
        var showExists = currentURL.search("show");
            showExists = parseInt(showExists);
        if(showExists > 0) {
            showGemsTab();
        }
        else {
            window.location.assign(homeURL + "?show=gems");
        }
    }
});
<?php
    if(isset($_GET['show'])) {
      if($_GET['show'] == "gems") {
      ?>
      jQuery(document).ready(function () {
       showGemsTab();
      });
      <?php
      }
      else if($_GET['show'] == "jewellery") {
      ?>
            jQuery(document).ready(function () {
      showJewelleryTab();
            });
      <?php
      }
    }
?>
var newsletterSignupInput = document.getElementById("newsletter-email");
newsletterSignupInput.addEventListener('keyup', sendForSignup);

//this function will send the user's email to a php script to be added onto the databse
function sendForSignup(event) {
//perform validation of the email
if (event['keyCode'] == 13) {

jQuery.ajax({
type: "POST",
url: newsletterURL,
data: {"newsletterEmail": newsletterSignupInput.value},
}).done(function(data) {
if (data == "true") {
alert("Thanks for signing up! You will receive an email from us soon! We promise not to spam you!");
//                    jQuery("<div>Thanks for signing up! You will receive an email from us soon! We promise not to spam you!</div>").dialog({
//
//                        modal: true,
//
//                    });
}
else {
alert("Sorry. Something went wrong on our end.");
}
//alert(data);
});

newsletterSignupInput.removeEventListener('keyup', sendForSignup);
}

}

</script>
<script src="<?php bloginfo('template_url') ?>/js/jquery-ui-1.10.4.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/jquery-ui-1.10.4.min.js"></script>
<script>
jQuery(function() {

jQuery('#mi-slider').catslider();

});
</script>






<!-- thumbnail scroller -->
<script>
/* jQuery.noConflict() for using the plugin along with other libraries.
You can remove it if you won't use other libraries (e.g. prototype, scriptaculous etc.) or
if you include jQuery before other libraries in yourdocument's head tag.
[more info: http://docs.jquery.com/Using_jQuery_with_Other_Libraries] */
jQuery.noConflict();
/* calling thumbnailScroller function with options as parameters */
(function(jQuery) {
window.onload = function() {
jQuery("#tS2").thumbnailScroller({
scrollerType: "hoverPrecise",
/* hoverPrecise, hoverAccelerate, clickButtons */

scrollerOrientation: "horizontal",
/* horizontal, vertical */

scrollSpeed: 0,
scrollEasing: "easeOutCirc",
/* Scroll easing type only for "hoverPrecise" type scrollers */

scrollEasingAmount: 100,
/* Scroll easing amount only for "hoverPrecise" and "clickButtons"
type scrollers (0 for no easing). */

acceleration: 2,
/* Acceleration value only for hoverAccelerate type\scrollers,
default: 2 */

scrollSpeed:500,
/* Scrolling speed only for "clickButtons" type scrollers,
values: milliseconds, */

noScrollCenterSpace: 0,
/* Scroller null scrolling area only for "hoverAccelerate"
type scrollers (0 being the absolute center of the scroller
and the default value), values: pixels */

autoScrolling: 1000,
/*Initial auto-scrolling (0 equals no auto-scrolling and
the default value), values: amount of auto-scrolling loops*/

autoScrollingSpeed: 30000,
/*Initial auto-scrolling speed, values: milliseconds*/

autoScrollingEasing: "linear",
/* Values for "scrollEasing" and "autoScrollingEasing" :
http://api.jqueryui.com/easings/
*/

autoScrollingDelay: 200
/* Initial auto-scrolling delay for each loop,
values: milliseconds */
});
}
})(jQuery);
</script>

<!-- thumbnailScroller script -->
<script src="<?php bloginfo('template_url') ?>/js/slide/jquery.thumbnailScroller.js"></script>
<!-- END thumbnail scroller -->

<script src="<?php bloginfo('template_url') ?>/js/modernizr.custom.06014.js"></script>
<script>Modernizr.load({
test: Modernizr.input.placeholder,
nope: ['Placeholder.js'],
complete: function() {
Placeholders.init();
}

});

</script>

<!-- pop-up script -->
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.browser.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/jquery.timeselector.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/classie.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/modalEffects.js"></script>

<!-- least.js JS-file -->
<!--
<script src="<?php bloginfo('template_url') ?>/js/least.min.js" defer="defer"></script>
-->
<!-- Lazyload JS-file -->
<!--
<script src="<?php bloginfo('template_url') ?>/js/jquery.lazyload.js" defer="defer"></script>
-->

<script>


jQuery('[class="time-selector"]').timeselector();


//jQuery(function() {

jQuery("#date-picker").datepicker({
changeMonth: true,
changeYear: true,
yearRange: '-100:+0',
});
// getter
//var dateFormat = jQuery("#date-picker").datepicker("option", "dateFormat");
// setter
//jQuery("#date-picker").datepicker("option", "dateFormat", "dd-mm-yy");
//});
</script>

<!-- END pop-up script -->

<!-- Home Latest Products Script -->
<script>
(function(jQuery) {
window.onload = function() {
swapitem1 = "#gallery li:nth-child(1)";
swapitem2 = "#gallery li:nth-child(2)";
swapitem3 = "#gallery li:nth-child(3)";
swapitem4 = "#gallery li:nth-child(4)";
swapitem5 = "#gallery li:nth-child(5)";
swapitem6 = "#gallery li:nth-child(6)";
swapitem7 = "#gallery li:nth-child(7)";
swapitem8 = "#gallery li:nth-child(8)";
swapitem9 = "#gallery li:nth-child(9)";
swapitem10 = "#gallery li:nth-child(10)";
var swaparray = new Array(swapitem1, swapitem2, swapitem3, swapitem4, swapitem5, swapitem6, swapitem7, swapitem8, swapitem9, swapitem10);


//swap1("#gallery li:nth-child(1)", "#gallery li:nth-child(5)");
setInterval(function() {
swap1(swaparray[Math.floor(Math.random() * swaparray.length)], swaparray[Math.floor(Math.random() * swaparray.length)]);
}, 3000);
}
}
)(jQuery);

function swap1(item1, item2) {

var thmb;
var src;
var path, path2;
var pathref, pathref2;
var name, name2;
var price, price2;

jQuery(item1 + " img").fadeOut(400, function() {
path = document.querySelector(item1 + " a");
pathref = jQuery(path).attr('href');

path2 = document.querySelector(item2 + " a");
pathref2 = jQuery(path2).attr('href');

h1 = document.querySelector(item1 + " div.infoLayer ul li h2");
h2 = document.querySelector(item2 + " div.infoLayer ul li h2");
h1title = h1.innerHTML;
h2title = h2.innerHTML;
getprice = document.querySelector(item1 + " div.infoLayer ul li p");
getprice2 = document.querySelector(item2 + " div.infoLayer ul li p");
price = getprice.innerHTML;
price2 = getprice2.innerHTML;
thmb = this;
src = this.src;
});

jQuery(item2 + " img").fadeOut(400, function() {

jQuery(path2).attr('href', pathref);
jQuery(item2 + " div.infoLayer ul li h2").html(h1title);
jQuery(item2 + " div.infoLayer ul li p").html(price);

thmb.src = this.src;
jQuery(this).fadeIn(400)[0].src = src;
});

jQuery(item1 + " img").fadeOut(400, function() {
jQuery(path).attr('href', pathref2);
jQuery(item1 + " div.infoLayer ul li h2").html(h2title);
jQuery(item1 + " div.infoLayer ul li p").html(price2);
thmb.src = this.src;
jQuery(this).fadeIn(400)[0].src = src;
});

}
</script>
<!-- End Home Latest Products Script -->


<!--Google Maps Scripts-->
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<script>
function initialize1() {
var location_map_srilanka = document.getElementById('location-map-srilanka');
var map_options = {
center: new google.maps.LatLng(44.5403, -78.5463),
zoom: 8,
mapTypeId: google.maps.MapTypeId.ROADMAP
}
var map = new google.maps.Map(location_map_srilanka, map_options)
var center = map.getCenter();
google.maps.event.trigger(map, 'resize');
map.setCenter(center);
}


google.maps.event.addDomListener(window, 'load', initialize1);
</script>
<script>
function initialize2() {
var location_map_japan = document.getElementById('location-map-japan');
var map_options = {
center: new google.maps.LatLng(55.5403, -60.5463),
zoom: 8,
mapTypeId: google.maps.MapTypeId.ROADMAP
}
var map = new google.maps.Map(location_map_japan, map_options)
var center = map.getCenter();
google.maps.event.trigger(map, 'resize');
map.setCenter(center);
}


google.maps.event.addDomListener(window, 'load', initialize2);
</script>
<script>
function initialize3() {
var location_map_us = document.getElementById('location-map-us');
var map_options = {
center: new google.maps.LatLng(45.5403, -75.5463),
zoom: 8,
mapTypeId: google.maps.MapTypeId.ROADMAP
}
var map = new google.maps.Map(location_map_us, map_options)
var center = map.getCenter();
google.maps.event.trigger(map, 'resize');
map.setCenter(center);
}


google.maps.event.addDomListener(window, 'load', initialize3);
</script>
<!--END Google Map Scripts-->


<!--Script for contact page-->
<script>
jQuery("#set-japan-location").click(function() {
if (jQuery("#location-srilanka").is(':visible')) {
jQuery("#set-srilanka-location").removeClass( "active" );
jQuery("#set-japan-location").addClass( "active" );
jQuery(".location-detials").hide();
jQuery("#location-japan").show();
initialize2();
}
if (jQuery("#location-us").is(':visible')) {
jQuery("#set-us-location").removeClass( "active" );
jQuery("#set-japan-location").addClass( "active" );
jQuery(".location-detials").hide();
jQuery("#location-japan").show();
initialize2();
}
});

jQuery("#set-srilanka-location").click(function() {

if (jQuery("#location-japan").is(':visible')) {
jQuery("#set-japan-location").removeClass( "active" );
jQuery("#set-srilanka-location").addClass( "active" );
jQuery(".location-detials").hide();
jQuery("#location-srilanka").show();
initialize1();
}

if (jQuery("#location-us").is(':visible')) {
jQuery("#set-us-location").removeClass( "active" );
jQuery("#set-srilanka-location").addClass( "active" );
jQuery(".location-detials").hide();
jQuery("#location-srilanka").show();
initialize1();
}


});

jQuery("#set-us-location").click(function() {

if (jQuery("#location-japan").is(':visible')) {
jQuery("#set-japan-location").removeClass( "active" );
jQuery("#set-us-location").addClass( "active" );
jQuery(".location-detials").hide();
jQuery("#location-us").show();
initialize3();
}

if (jQuery("#location-srilanka").is(':visible')) {
jQuery("#set-srilanka-location").removeClass( "active" );
jQuery("#set-us-location").addClass( "active" );
jQuery(".location-detials").hide();
jQuery("#location-us").show();
initialize3();
}
});
</script>


<!--[if gte IE 9]>
<style type="text/css">
.gradient {
filter: none;
}
</style>
<![endif]-->
</body>
</html>