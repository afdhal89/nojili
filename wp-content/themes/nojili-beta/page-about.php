<?php get_header(); ?>
<div class="clear"></div>
<section id="about-page">
<div id="founding-story">
    <h1>A Founding Story</h1>
    <img src="<?php bloginfo('template_url'); ?>/images/founder.jpg" alt="" />
    <p>Mr. L. A. D Palitha, head of Nojili Export Import Pvt Ltd, is a Graduate in the field of Gem and Jewelry and was proud to take over the business after his own father. The Company was officially registered in Sri Lanka in 1995 and grew from strength to strength expanding into new markets.
</p>
<p>
Nojili Export Import Pvt Ltd deals mainly in Gems and Jewelry. We believe in custom jewelry that is unique and strive to bring out the best quality of Gems and Jewelry that can be achieved. The products are exported directly from the mines in to the hands of our customers. We are a company with a rich and unique history, birthed in the mines of this beautiful Island, Sri Lanka. Established in the late 1970's, we have continued to bring the best quality of gems and handmade Jewelry to our wide range of clients.
</p>
<p>
Currently Nojili Export Import Pvt Ltd has affiliations in USA, Canada, Malaysia, and Japan. We have even moved into new realms of the export business and have introduced Mineral Sands as a key product in our lineup. Currently we export Zircon, Rutile and Ilmenite to customers in the Asian Market and hope to expand our customer base in the near future.
</p>
<p>
Transparency is highly valued among our customers and we strive to be above the rest by introducing them to 100% natural gemstones which are rare in the current market as more and more gems are treated with heat and other external factors. As such every gem we sell is accompanied by a Guarantee to certify the Gem to be a natural gem with no treatment whatsoever.  We also provide all the internationally recognized laboratory documents as well as samples of our Mineral Sands products.
</p>
<p>
We at Nojili Export Import Pvt Ltd promise the best service possible in bringing you the unique, quality products that you require.
</p>
</div>
<div class="clear"></div>
<div class="divider-holder"><div class="divider"></div></div>
<div class="clear"></div>

<div id="infographic">
<div id="section-01"></div>
<div id="section-02">
    <div>
    <div class="ellipe-container">
    <img src="<?php bloginfo('template_url'); ?>/images/infographic/ellipse.png" class="ellipse" alt="" />
    <img class="pop-up" src="<?php bloginfo('template_url'); ?>/images/infographic/img/08.jpg" title="large image" />
    </div>
    <div class="ellipe-container">
    <img src="<?php bloginfo('template_url'); ?>/images/infographic/ellipse.png" class="ellipse" alt="" />
    <img class="pop-up" src="<?php bloginfo('template_url'); ?>/images/infographic/img/10.jpg" title="large image" />
    </div>
    <div class="ellipe-container">
    <img src="<?php bloginfo('template_url'); ?>/images/infographic/ellipse.png" class="ellipse" alt="" />
    <img class="pop-up" src="<?php bloginfo('template_url'); ?>/images/infographic/img/12.jpg" title="large image" />
    </div>
    </div>

        <div>
    <div class="ellipe-container">
    <img src="<?php bloginfo('template_url'); ?>/images/infographic/ellipse.png" class="ellipse" alt="" />
    <img class="pop-up" src="<?php bloginfo('template_url'); ?>/images/infographic/img/15.jpg" title="large image" />
    </div>
    <div class="ellipe-container">
    <img src="<?php bloginfo('template_url'); ?>/images/infographic/ellipse.png" class="ellipse" alt="" />
    <img class="pop-up" src="<?php bloginfo('template_url'); ?>/images/infographic/img/16.jpg" title="large image" />
    </div>
    <div class="ellipe-container">
    <img src="<?php bloginfo('template_url'); ?>/images/infographic/ellipse.png" class="ellipse" alt="" />
    <img class="pop-up" src="<?php bloginfo('template_url'); ?>/images/infographic/img/18.jpg" title="large image" />
    </div>
    </div>
</div>
<div id="section-03">
    <div>
    <div class="ellipe-container">
    <img src="<?php bloginfo('template_url'); ?>/images/infographic/ellipse.png" class="ellipse" alt="" />
    <img class="pop-up" src="<?php bloginfo('template_url'); ?>/images/infographic/img/07.jpg" title="large image" />
    </div>
    <div class="ellipe-container">
    <img src="<?php bloginfo('template_url'); ?>/images/infographic/ellipse.png" class="ellipse" alt="" />
    <img class="pop-up" src="<?php bloginfo('template_url'); ?>/images/infographic/img/17.jpg" title="large image" />
    </div>
    <div class="ellipe-container">
    <img src="<?php bloginfo('template_url'); ?>/images/infographic/ellipse.png" class="ellipse" alt="" />
    <img class="pop-up" src="<?php bloginfo('template_url'); ?>/images/infographic/img/18.jpg" title="large image" />
    </div>
    </div>
</div>
<div id="section-04"></div>
<div id="section-05">
        <div>
    <div class="ellipe-container">
    <img src="<?php bloginfo('template_url'); ?>/images/infographic/ellipse.png" class="ellipse" alt="" />
    <img class="pop-up" src="<?php bloginfo('template_url'); ?>/images/infographic/img/04.jpg" title="large image" />
    </div>
    <div class="ellipe-container">
    <img src="<?php bloginfo('template_url'); ?>/images/infographic/ellipse.png" class="ellipse" alt="" />
    <img class="pop-up" src="<?php bloginfo('template_url'); ?>/images/infographic/img/03.jpg" title="large image" />
    </div>
    <div class="ellipe-container">
    <img src="<?php bloginfo('template_url'); ?>/images/infographic/ellipse.png" class="ellipse" alt="" />
    <img class="pop-up" src="<?php bloginfo('template_url'); ?>/images/infographic/img/02.jpg" title="large image" />
    </div>
    </div>
    </div>
</div>
</section>
<?php get_footer(); ?>
