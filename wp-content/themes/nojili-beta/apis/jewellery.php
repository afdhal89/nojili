<?php
    /* This API script generates a JSON file of all the jewellery items in the database */
    require_once '../../../../wp-load.php';
    require_once './medoo.php';

    $db = new medoo('nojilipro');

    $allJewellery = $db->select("item", [
            
            "[><]image" => ["image_id" => "id"]
            ],
            
            "*", [
                
            "class_1"=>"Jewellery"
    ]);

    $jsonString = json_encode($allJewellery);

    //Write JSON output to file
    $outputFile = "../../../../data/jewellery_list.json";
    
    //if the file doesn't exist, create it
    if(!file_exists($outputFile)){
        touch($outputFile);
        chmod($outputFile, 0666);
    }

    //write the json data into the file
    $fp = fopen($outputFile, "w");
    fwrite($fp, $jsonString);
    fclose($fp);

    echo 'jewellery_list.json has been generated!';
?>
