<?php
require 'jsonGenerator.php';
$displayResults = false;
$data;
global $databaseObject;

if($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST["search"])) {
        
        //$sqlCon = new medoo('nojilipro');
        $data = $databaseObject->select("item", ["[><]image" => ["image_id" => "id"]], "*", ['LIKE' => ["".$_POST["criteria"] => "".$_POST["query"]]]);
        //var_dump($data);
        //var_dump($sqlCon->last_query());
        $displayResults = true;
        //var_dump($_POST);
    }
    if(isset($_POST["edit"])) {
        var_dump($_POST);   
    }
}
?>
<div id="update-items">
<div id="header">
    <form action="#" method="POST">
        <label>Search by: </label>
        <select name="criteria">
            <option value="name">Name</option>
<!--            <option value="serial_number">Serial Number</option>-->
        </select>
        <br/>
        <input name="query" value="" type="text" placeholder="Your Query"/>
        <input name="search" value="true" type="hidden"/>
        <input id="search-button" name="submit" value="Search" type="submit"/>
    </form>
</div>

<div id="searchResults">
    

<?php

if($_SERVER["REQUEST_METHOD"] == "POST") {
    
    if(isset($_POST["search"])) {

        echo '<form action="#" method="POST">';
        echo '<table id="showQueriesTable">';
        echo '<tr>';
        echo '<th>ID</th>';
        echo '<th>Name</th>';
        echo '<th>Qty. In Stock</th>';
        echo '<th>Karat</th>';
        echo '<th>Stone Description</th>';
        echo '<th>Price</th>';
        echo '<th>Serial Number</th>';
        echo '<th>Image</th>';
        echo '<th>Class 1</th>';
        echo '<th>Class 2</th>';
        echo '<th>Select</th>';
        echo '</tr>';

        foreach($data as $data) {

            echo '<tr>';
            echo '<td>' . $data["id"] . '</td>';
            echo '<td>' . $data["name"] . '</td>';
            echo '<td>' . $data["quantity_in_stock"] . '</td>';
            echo '<td>' . $data["karat"] . '</td>';
            echo '<td>' . $data["stone_description"] . '</td>';
            echo '<td>' . $data["price"] . '</td>';
            echo '<td>' . $data["serial_number"] . '</td>';
            echo '<td><img id="showInformationTableItemImage" src="' . $data["url"] . '" width="50px"/></td>';
            echo '<td>' . $data["class_1"] . '</td>';
            echo '<td>' . $data["class_2"] . '</td>';
            echo '<td><input type="radio" name="selectedForEdit" value="' . $data["serial_number"] . '" onclick="enableButtons()"/></td>';
            echo '</tr>';
        } 
        echo '</table>';
        echo '<input id="edit" name="edit" value="Edit" type="submit" disabled/>';
        echo '<input id="delete" name="delete" value="Delete" type="submit" disabled/>';
        echo '</form>';
    }

    else if(isset($_POST["delete"])) {
        //var_dump($_POST);
        if(isset($_POST['selectedForEdit'])) {
            $queryResults = $databaseObject->delete("item", ["serial_number" => "".$_POST['selectedForEdit']]);    
            //var_dump($databaseObject->last_query());
            
            if($queryResults > 0) {
                echo "<h2>Sucessfully deleted item</h2>";
            }
            else {
                echo "<h2>There was an error deleting the item</h2>";
            }
        }
    }
    
    else if(isset($_POST["edit"])) {
        $queryResults = $databaseObject->select("item", ["[><]image" => ["image_id" => "id"]], "*", ["serial_number" => "".$_POST["selectedForEdit"]]);
        //var_dump($sqlCon->last_query());
        //var_dump($sqlCon->error());
        //var_dump($queryResults);
        $_SESSION["previousImageURL"] = $queryResults[0]['url'];
        $_SESSION["previousItemSerial"] = $_POST["selectedForEdit"];
        
?>
</div>
</div>
<div id="edit-items">
            <h3>---- Edit Information ----</h3>
<form name="itemDetails" action="#" method="POST" enctype="multipart/form-data">
            <h4>Product Image</h4>

            <img src="<?php if (!empty($queryResults)) echo $queryResults[0]['url']; ?>" alt="" width="50px"/>
<br/>
 <label id="image-description">Set a New Image : </label><input name="fileUpload" type="file" /><br/>
            <label id="image-description">Image Description : </label><input type="text" name="imageDescription" value="<?php
            if (!empty($queryResults)) echo $queryResults[0]['description'];
            ?>"><br/>
            <hr/>
            <h4>Product Details</h4>
            <label>Item Name :</label>
            <input type="text" name="itemName" value="<?php if (!empty($queryResults)) echo $queryResults[0]['name']; ?>"><br/>

            <label>Quantity in stock :</label></label><input type="text" name="quantity" value="<?php if (!empty($queryResults)) echo        $queryResults[0]['quantity_in_stock']; ?>"/><br/>

            <label>Karat :</label><input type="text" name="karat" value="<?php if (!empty($queryResults)) echo $queryResults[0]['karat']; ?>"/><br/>

            <label>Stone Description :</label><input type="text" name="stoneDescription" value="<?php
                if (!empty($queryResults)) echo $queryResults[0]['stone_description']; 
            ?>"/><br/>

            <label>Price :</label><input type="text" name="price" value="<?php if (!empty($queryResults)) echo $queryResults[0]['price'];  ?>"/><br/>

            <label>Serial Number : </label><input type="text" name="serialNumber" value="<?php
            if (!empty($queryResults)) echo $queryResults[0]['serial_number']; 
            ?>"/><br/>

            <label>Class 1 :</label>
            <select id="class1" name="class1" value=""/>
            <option value="gem">Gem</option>
            <option value="jewellery">Jewellery</option>
            </select>
            <br/>

            <label>Class 2 :</label>
            <select id="class2_jewellery" name="class2Jewellery" value="" style="display:none;"/>
            <option value="ring">Ring</option>
            <option value="pendant">Pendant</option>
            </select>

            <select id="class2_gems" name="class2Gems" value=""/>
            <option value="unheated">Unheated</option>
            <option value="heated">Heated</option>
            </select>
            <br/>

            <input type="submit" name="update" value="Update"/>

        </form>
</div>
        <script>
            console.log("running script");
            var class1Selector = document.getElementById("class1");
            var gemSelector = document.getElementById("class2_gems");
            var jewellerySelector = document.getElementById("class2_jewellery");

            class1Selector.addEventListener('change', function(event) {
                console.log("focus out detected");
                console.log(event.target.value);
                if (event.target.value == "gem") {
                    console.log(gemSelector.style);
                    gemSelector.style.display = "inline";
                    jewellerySelector.style.display = "none";
                }
                else if (event.target.value == "jewellery") {
                    console.log(jewellerySelector.style);
                    jewellerySelector.style.display = "inline";
                    gemSelector.style.display = "none";
                }

            });
       </script>
<?php   
    }
    else if(isset($_POST["update"])) {
        //update information
        //var_dump($_POST);
        //var_dump($_FILES);
        
        if(($_FILES['fileUpload']['name']) != '') {
            echo "in second if";
            //unlink("" . $_SESSION["previousImageURL"]);
            
            //check if there is an error in the file upload
            if (!empty($_FILES['fileUpload']['error'])) {
                echo 'an error occured<br/>';
            } 
            else {

                //echo 'no errors, proceed..<br/>';
                //get the file extention from the file name
                $fileType = pathinfo('' . $_FILES['fileUpload']['name'], PATHINFO_EXTENSION);

                //check if the uploaded file is a png or a jpg strcmp() is used to compare strings
                if ((strcmp($fileType, "png") != "0") && (strcmp($fileType, "jpg") != "0")) {
                    //show an error if the file isnt an excepted type
                    echo 'file isnt an excepted type<br/>';
                } else {

                    //echo 'file type is OK. Proceed: ' . $fileType . '<br/>';
                    //set permanent location for the image file
                    $permLoc = '../wp-content/image_dir/' . $_FILES['fileUpload']['name'];

                    //if the file type is acceptable, move the uploaded file to a premanent location
                    if (move_uploaded_file('' . $_FILES['fileUpload']['tmp_name'], $permLoc)) {
                        //set the image url into the send to database array
                        $_SESSION['newImageUrl'] = $permLoc;
                        //set the image description into the send to database array
                        $_SESSION['newImageDescription'] = $_POST['imageDescription'];

                        //tell the user the file has been uploaded
                        echo 'file has been uploaded!';
                        
                        $success = $databaseObject->update("image", [
                                        "url" => $permLoc,
                                        "description" =>  $_POST['imageDescription']
                                    ], [
                                        "url" => $_SESSION["previousImageURL"],
                                    ]);
                        
                        //var_dump($sqlCon->last_query());
                        //var_dump($sqlCon->error());
                        //var_dump($success);
                        
                        if(!empty($success)) {
                            echo "<p>The new file has been uploaded</p>";   
                        }
                    }
                }
            }
        }
        
        if($_POST["class1"] == "gem") {
            $class2 = $_POST["class2Gems"];
        }
        else if($_POST["class1"] == "jewellery") {
            $class2 = $_POST["class2Jewellery"];
        }

        $success = $databaseObject->update("item", [
                                    "name" => $_POST['itemName'],
                                    "stone_description" =>  $_POST['imageDescription'],
                                    "quantity_in_stock" =>  $_POST['quantity'],
                                    "karat" =>  $_POST['karat'],
                                    "price" =>  $_POST['price'],
                                    "serial_number" =>  $_POST['serialNumber'],
                                    "class_1" =>  $_POST['class1'],
                                    "class_2" =>  $class2,
                                ], [
                                    "serial_number" => $_SESSION["previousItemSerial"],
                                ]);
        
        if(!empty($success)) {
            generateJewelleryJson();
            generateGemsJson();
            generateLatestTenItems();
            
            echo "<h3>Item " . $_POST['itemName'] . " has been updated!</h3>";
        }
    }
}
?>
<script>
            function enableButtons() {
                jQuery("#edit").prop('disabled', false);
                jQuery("#delete").prop('disabled', false);
            }

</script>