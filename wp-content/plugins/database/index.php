<?php
/*
Plugin Name: Database Editor
Plugin URI: 
Description: Menu to access and edit database entries
Author: Deleepa Perera
Author URI: http://deleepa.me
*/

/*Add the database object as a global variable*/
//require 'libs/medoo.php';
session_start();

/* An activation hook to create the folder for image uploads */
register_activation_hook('database/index.php', 'onActivate');

/* Adds all the necessary menu items */
add_action('admin_menu', 'addMainMenuEntry');
add_action('admin_menu', 'addSubmenus');

/* Register the CSS and JS files necessary */
add_action('admin_enqueue_scripts', 'loadJSAndCSS');

/* Registers and loads the JS and CSS files */
function loadJSAndCSS() {
 
    wp_register_style('database_plugin_stylesheet', plugins_url() . '/database/style.css');
    wp_enqueue_style('database_plugin_stylesheet');
    
}


/* Creates a new menu item under the admin menus */
function addMainMenuEntry() { 
    add_menu_page( "Database Editor", "Database Editor", "administrator", "db_editor", "helloWorld");

}

/* This function adds all the submenus */
function addSubmenus() {
 
    add_submenu_page("db_editor", "Add a new Entry", "New Entry", "administrator", "db_editor_new_entry", "addNewEntry");
    add_submenu_page("db_editor", "Lookup Items", "Lookup Items", "administrator", "db_editor_lookup", "lookup");
    add_submenu_page("db_editor", "Update Items", "Update Items", "administrator", "db_editor_edit", "databaseEditor");
    add_submenu_page("db_editor", "Customer Lookup", "Customer Lookup", "administrator", "db_editor_customer_lookup", "customerLookup");
    
}

/*Plugin main menu entry constructor*/
function helloWorld() {
    require_once 'start_page.php';
}

/* Displays the page to add a new item to the database */
function addNewEntry() {
    require 'newEntry.php';
    initNewEntry();
}

/* Displays the page that shows all the items in the database */
function lookup() {
    require 'showInformation.php';
}

/* Displays the page that shows all the items in the database */
function databaseEditor() {
    require 'databaseEditor.php';
}

function customerLookup() {
    require 'showCustomers.php';
}

/* This function runs on plugin activation */
/* Create the necessary files and folders here */
function onActivate() {
    mkdir('../wp-content/image_dir');
    mkdir('../wp-content/json_data');
}
