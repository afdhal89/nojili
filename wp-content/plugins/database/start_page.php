<h1>Start Page</h1>
<p>Hello and welcome to the database editor for the Nojili website. This is your central hub and this is where you will be able to manage what items are displayed on your website.</p>
<p>If you're not sure how to use this section of the website, we recommend reading the user manual that was emailed to you.</p>
<p>In case you've misplaced it, you can download it at the following link: </p>
<a href="../wp-content/plugins/database/user_manual_for_nojili.pdf">Download here</a>
<p>To save the file, right click on the link and click on "Save linked file as.."</p>