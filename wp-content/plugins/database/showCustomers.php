<?php

//$sqlCon = new medoo("nojilipro");
global $databaseObject;

function printTableHeader() {
    echo '<table id="showInformationTable">';
    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>Name</th>';
    echo '<th>Email</th>';
    echo '</tr>';
}

function printTableContent($data) {
    
        echo '<tr>';
        echo '<td>' . $data["id"] . '</td>';
        echo '<td>' . $data["name"] . '</td>';
        echo '<td>' . $data["email"] . '</td>';
        echo '</tr>';
}

function printTableFooter() {
    echo '</table>';   
}

$queryResult = $databaseObject->select("customer", "*");
echo '<div id="lookup-customers">';

printTableHeader();
foreach($queryResult as $queryResult) {
    $queryResult['name'] = "" . $queryResult['first_name'] . $queryResult['last_name'];
    printTableContent($queryResult);
}
printTableFooter();
echo '</div>'; //end lookup customers
?>