<?php
    /* This API script generates a JSON file of all the gem items in the database */
    //require_once '../../../../wp-load.php';
    require_once 'medoo.php';

    $db = new medoo('nojilipro');

    $allJewellery = $db->select("item", [
        
        "[><]image" => ["image_id" => "id"]
        ],
        "*", [
        "class_1"=>"gem"
    ]);

    $jsonString = json_encode($allJewellery);

    //Write JSON output to file
    $outputFile = "../../json_data/gem_list.json";

    //if the file doesn't exist, create it
    if(!file_exists($outputFile)){
        touch($outputFile);
        chmod($outputFile, 0666);
    }

    //write the json data into the file
    $fp = fopen($outputFile, "w");
    fwrite($fp, $jsonString);
    fclose($fp);

    echo 'gem_list.json has been generated!';
?>
