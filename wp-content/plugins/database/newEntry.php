<?php
//require 'medoo.php';
require 'jsonGenerator.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    //if the request is POST, start the session 
    //var_dump($_POST);
    //var_dump($_POST['fileUpload']);
    if (!empty($_POST['type'])) {
        //check if the image upload form has been submitted
        if ($_POST['type'] == "imageUpload") {
            //check if a file has been uploaded to the server
            if (!empty($_FILES)) {

                //echo 'file upload detected<br/>';
                //var_dump($_FILES);
                //check if there is an error in the file upload
                if (!empty($_FILES['fileUpload']['error'])) {
                    echo '<p class="new_entry_error">Sorry an error occured on the server.</p><br/>';
                } else {
                    
                    //echo 'no errors, proceed..<br/>'; 
                    //get the file extention from the file name
                    $fileType = pathinfo('' . $_FILES['fileUpload']['name'], PATHINFO_EXTENSION);

                    //check file size
                    //var_dump($_FILES);
                    //if($_FILES['fileUpload']['size'] > 999999) {
                      //  echo '<p class="new_entry_error">The file size is too large.</p><br/>';
                    //}
                    //else {
                        //check if the uploaded file is a png or a jpg strcmp() is used to compare strings
                        if ((strcmp($fileType, "png") != "0") && (strcmp($fileType, "jpg") != "0")) {
                            //show an error if the file isnt an excepted type
                            echo '<p class="new_entry_error">Error: File isnt an excepted type</p><br/>';
                        } else {

                            //echo 'file type is OK. Proceed: ' . $fileType . '<br/>';
                            //set permanent location for the image file
                            $permLoc = '../wp-content/image_dir/' . $_FILES['fileUpload']['name'];

                            //if the file type is acceptable, move the uploaded file to a premanent location
                            if (move_uploaded_file('' . $_FILES['fileUpload']['tmp_name'], $permLoc)) {
                                //set the image url into the send to database array
                                $_SESSION['imageUrl'] = $permLoc;
                                //set the image description into the send to database array
                                $_SESSION['imageDescription'] = $_POST['imageDescription'];

                                //tell the user the file has been uploaded
                                echo '<p class="new_entry_success">Success: File has been uploaded!</p></br>';
                            }
                        }
                    //}
                }
            }
        } else if ($_POST['type'] == "itemDetails") {
            //check if image has been uploaded
            if(!isset($_SESSION['imageUrl']) || !isset($_SESSION['imageDescription'])) {
                echo '<p class="new_entry_error">You must upload an image before proceeding!</p><br/>';
            }
            else {
                //echo '<h3>Post:</h3>';
                //var_dump($_POST);

    //            $sqlCon = new medoo('nojilipro');
                global $databaseObject;
                //echo 'sql connection established';



                if ($_POST['class1'] == "jewellery") {
                    $class2Value = $_POST['class2Jewellery'];
                } else if ($_POST['class1'] == "gem") {
                    $class2Value = $_POST['class2Gems'];
                }

                $imageId = $databaseObject->insert('image', [

                    "url" => $_SESSION['imageUrl'],
                    "description" => $_SESSION['imageDescription']
                ]);

                $databaseObject->insert('item', [
                    "name" => $_POST['itemName'],
                    "quantity_in_stock" => $_POST['quantity'],
                    "karat" => $_POST['karat'],
                    "stone_description" => $_POST['stoneDescription'],
                    "price" => $_POST['price'],
                    "serial_number" => $_POST['serialNumber'],
                    "image_id" => $imageId,
                    "class_1" => $_POST['class1'],
                    "class_2" => $class2Value,
                ]);

                //var_dump($databaseObject->last_query());
                //var_dump($databaseObject->error());
                $errorDump = $databaseObject->error();
                $errorExists;
                
                if($errorDump[0] == '00000') {
                    $errorExists = false;
                }
                else {
                    $errorExists = true;
                }
    
                //var_dump($databaseObject->last_query());
                
                generateJewelleryJson();
                generateGemsJson();
                generateLatestTenItems();

                session_destroy();
            }
        }
    }
}

/* HTML output for the database menu */

function initNewEntry() {
    print "<div id='new-entry'>"
            . "<h1 id='plugin-title'>Database Menu Options</h1>";
    //print 
    ?>

    <?php
        //show success message
        if(isset($errorExists) && $errorExists == false) {
    ?>

        <div id="statusMessage">
            <h2>The information was saved sucessfully. You can view the information through the Lookup Item's section.</h2>
        </div>

    <?php
        }
        else if (isset($errorExists) && $errorExists == true) {
    ?>
        <div id="statusMessage">
            <h2>There was an error with the database.</h2>
            <p>Error code: <?php  echo $errorDump[0]?></p>
            <p><?php  echo $errorDump[1]?></p>
            <p><?php  echo $errorDump[2]?></p>
        </div>
    <?php
        }
    ?>

    <!--Step 01 - Upload the image -->
    <div id="step-1">

        <form id="imageUpload" name="imageUpload" action="#" method="POST" enctype="multipart/form-data">
            <h3>Step 1: Upload the image</h3>
            <input name="fileUpload" type="file"/><br/>
            <label id="image-description"><p>Image Description : </p></label><input type="text" name="imageDescription" value="<?php
            if (isset($_SESSION['imageDescription']))
                echo $_SESSION['imageDescription'];
            ?>"><br/>
            <input type="submit" value="Upload" /><br/>
            <input type="hidden" name="type" value="imageUpload"/>
        </form>

        <div>
            <img src="<?php if (isset($_SESSION['imageUrl'])) echo $_SESSION['imageUrl']; ?>" alt="">
        </div>

    </div>
    <!-- END Step 01 - Upload the image -->

    <div class="clear"></div>

    <!-- Step 2:  Enter the details -->
    <div id="step-2">
        <form name="itemDetails" action="#" method="POST">
            <h3>Step 2:  Enter the details</h3>
            <label><p>Item Name : </p></label>
            <input type="text" name="itemName" value="<?php if (isset($_SESSION['itemName'])) echo $_SESSION['itemName']; ?>"><br/>

            <label><p>Quantity in stock : </p></label></label><input type="text" name="quantity" value="<?php if (isset($_SESSION['quantity'])) echo $_SESSION['quantity']; ?>"/><br/>

            <label><p>Karat : </p></label><input type="text" name="karat" value="<?php if (isset($_SESSION['karat'])) echo $_SESSION['karat']; ?>"/><br/>

            <label><p>Stone Description : </p></label><input type="text" name="stoneDescription" value="<?php
            if (isset($_SESSION['stoneDescription']))
                echo $_SESSION['stoneDescription'];
            ?>"/><br/>

            <label><p>Price : </p></label><input type="text" name="price" value="<?php if (isset($_SESSION['price'])) echo $_SESSION['price']; ?>"/><br/>

            <label><p>Serial Number : </p></label><input type="text" name="serialNumber" value="<?php
            if (isset($_SESSION['serialNumber']))
                echo $_SESSION['serialNumber'];
            ?>"/><br/>

            <label><p>Class 1 : </p></label>
            <select id="class1" name="class1" value="<?php if (isset($_SESSION['class1'])) echo $_SESSION['class1']; ?>"/>
            <option value="gem">Gem</option>
            <option value="jewellery">Jewellery</option>
            </select>
            <br/>

            <label><p>Class 2 : </p></label>
            <select id="class2_jewellery" name="class2Jewellery" value="" style="display:none;"/>
            <option value="ring">Ring</option>
            <option value="pendant">Pendant</option>
            </select>

            <select id="class2_gems" name="class2Gems" value=""/>
            <option value="unheated">Unheated</option>
            <option value="heated">Heated</option>
            </select>
            <br/>

            <input type="submit" name="submit" value="Confirm and Save"/>
            <input type="hidden" name="type" value="itemDetails"/>

        </form>
    </div>
    <!-- END Step 2:  Enter the details --> 

    <script>
        console.log("running script");
        var class1Selector = document.getElementById("class1");
        var gemSelector = document.getElementById("class2_gems");
        var jewellerySelector = document.getElementById("class2_jewellery");

        class1Selector.addEventListener('change', function(event) {
            console.log("focus out detected");
            console.log(event.target.value);
            if (event.target.value == "gem") {
                console.log(gemSelector.style);
                gemSelector.style.display = "inline";
                jewellerySelector.style.display = "none";
            }
            else if (event.target.value == "jewellery") {
                console.log(jewellerySelector.style);
                jewellerySelector.style.display = "inline";
                gemSelector.style.display = "none";
            }

        });

    </script>

    <?php
    print "</div>";
}
?>