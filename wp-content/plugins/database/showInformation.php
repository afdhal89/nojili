<?php

//$sqlCon = new medoo("nojilipro");
global $databaseObject;

function printTableHeader() {
    echo '<table id="showInformationTable">';
    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>Name</th>';
    echo '<th>Qty. in Stock</th>';
    echo '<th>Karat</th>';
    echo '<th>Stone Description</th>';
    echo '<th>Price</th>';
    echo '<th>Serial Number</th>';
    echo '<th>Image</th>';
//    echo '<th>Class 1</th>';
    echo '<th>Class 2</th>';
    echo '</tr>';
}

function printTableContent($data) {
    
        echo '<tr>';
        echo '<td>' . $data["id"] . '</td>';
        echo '<td>' . $data["name"] . '</td>';
        echo '<td>' . $data["quantity_in_stock"] . '</td>';
        echo '<td>' . $data["karat"] . '</td>';
        echo '<td>' . $data["stone_description"] . '</td>';
        echo '<td>' . $data["price"] . '</td>';
        echo '<td>' . $data["serial_number"] . '</td>';
        echo '<td><img id="showInformationTableItemImage" src="' . $data["url"] . '" width="50px"/></td>';
        //echo '<td>' . $data["class_1"] . '</td>';
        echo '<td>' . $data["class_2"] . '</td>';
        echo '</tr>';
}

function printTableFooter() {
    echo '</table>';   
}

$queryResult = $databaseObject->select("item", ["[><]image" => ["image_id" => "id"]],"*",["ORDER" => "item.id DESC"]);
echo '<div id="lookup-items">';

echo '<div id="lookup-gems">';
echo '<h2>Gems</h2>';
echo '<div id="lookup-gems-container">';
printTableHeader();
foreach($queryResult as $queryResult) {
    if($queryResult['class_1'] == "gem") {
        printTableContent($queryResult);
    }
}
printTableFooter();
echo '</div>'; //end lookup-gems-container
echo '</div>';// end lookup-gems

$queryResult2 = $databaseObject->select("item", ["[><]image" => ["image_id" => "id"]],"*",["ORDER" => "item.id DESC"]);

echo '<div id="lookup-jewellery">';
echo '<h2>Jewellery</h2>';
echo '<div id="lookup-jewellery-container">';
printTableHeader();
foreach($queryResult2 as $queryResult2) {
    if($queryResult2['class_1'] == "jewellery") {
        printTableContent($queryResult2);
    }
}
printTableFooter();
echo '</div>'; // end lookup-jewellery-container
echo '</div>';// end lookup-jewellery

echo '</div>';// end lookup-items
?>
<script>
var flip = 0;
jQuery("#lookup-gems h2").click(function() {
    jQuery("#lookup-gems-container").toggle( flip++ % 2 === 0 );

});

jQuery("#lookup-jewellery h2").click(function() {
    jQuery("#lookup-jewellery-container").toggle( flip++ % 2 === 0 );

});
</script>