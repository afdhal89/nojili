<?php
/* This API script generates a JSON file of all the gem items in the database */
//require_once '../../../../wp-load.php';

//require_once 'medoo.php';

function generateJewelleryJson() {
    global $databaseObject;
    
    $allJewellery = $databaseObject->select("item", ["[><]image" => ["image_id" => "id"]], "*", ["class_1"=>"jewellery"]);

    $jsonString = json_encode($allJewellery);

    //Write JSON output to file
    $outputFile = "../wp-content/json_data/allJewellery.json";

    //if the file doesn't exist, create it
    if(!file_exists($outputFile)){
        touch($outputFile);
        chmod($outputFile, 0666);
    }

    //write the json data into the file
    $fp = fopen($outputFile, "w");
    fwrite($fp, $jsonString);
    fclose($fp);

    return $databaseObject->last_query();
}

function generateGemsJson() {
       
    global $databaseObject;
    
    $allJewellery = $databaseObject->select("item", ["[><]image" => ["image_id" => "id"]],"*", ["class_1"=>"gem"]);

    $jsonString = json_encode($allJewellery);

    //Write JSON output to file
    $outputFile = "../wp-content/json_data/allGems.json";
    
    //if the file doesn't exist, create it
    if(!file_exists($outputFile)){
        touch($outputFile);
        chmod($outputFile, 0666);
    }

    //write the json data into the file
    $fp = fopen($outputFile, "w");
    fwrite($fp, $jsonString);
    fclose($fp);

    return $databaseObject->last_query();
    
}

function generateLatestTenItems() {
    
    global $databaseObject;
    $counter = 0;
    
    $maxId = $databaseObject->max("item", "id");
    $idToGetFrom = $maxId - 10;
    //$jsonString = "[";
    
    //loop
    do {
        $temp = $databaseObject->select("item", ["[><]image" => ["image_id" => "id"]], "*", ["item.id" => $maxId]);    
        if($temp != null) {
            //$jsonString += "{";
            //$jsonString += json_encode($temp);
            //$jsonString += "}";
            $allJewellery[$counter] = $temp[0];    
            $counter++;
        }
        if($maxId == 0) {
            break;
        }
        $maxId--;
    }while($counter < 10);
    
    //$jsonString += "]";

    $jsonString = json_encode($allJewellery);
    
    //Write JSON output to file
    $outputFile = "../wp-content/json_data/latestTenItems.json";
    
    //if the file doesn't exist, create it
    if(!file_exists($outputFile)){
        touch($outputFile);
        chmod($outputFile, 0666);
    }

    //write the json data into the file
    $fp = fopen($outputFile, "w");
    fwrite($fp, $jsonString);
    fclose($fp);

    //return $databaseObject->last_query();
    return $allJewellery;
    
}
?>
